'use strict';

var myHippocratesApp = angular.module('myHippocratesApp', ['ngRoute']);

myHippocratesApp.config(['$routeProvider', function($routeProvider) {
   $routeProvider
	.when('/indexPage', {
	templateUrl: '/index.html',
	controller: 'loginCtrl'
   })
	.when('/homePage', {
	templateUrl: '/home.html',
	controller: 'homeCtrl'
   })
	.when('/homeForPatientPage', {
	templateUrl: '/home_patient.html',
	controller: 'homePatientCtrl'
   })
	.when('/schedulePage', {
	templateUrl: '/schedule.html',
	controller: 'scheduleCtrl'
   })
	.when('/workFlowPage', {
	templateUrl: '/work_flow.html',
	controller: 'workFlowCtrl'
   })
	.when('/patientListPage', {
	templateUrl: '/patient_list.html',
	controller: 'patientDataCtrl'
   })
	.when('/securityPage', {
	templateUrl: '/security.html',
	controller: 'securityCtrl'
   })
	.when('/decisionSupportPage', {
	templateUrl: '/decision_support.html',
	controller: 'decisionSupportCtrl'
   })
	.when('/myAppsPage', {
	templateUrl: '/my_apps.html',
	controller: 'myAppsCtrl'
   })
	.when('/settingsPage', {
	templateUrl: '/settings.html',
	controller: 'settingsCtrl'
   })
	.when('/AboutPage', {
	templateUrl: '/about.html',
	controller: 'aboutCtrl'
   })
	.when('/newsFeedPage', {
	templateUrl: '/news_feed.html',
	controller: 'newFeedCtrl'
   })
	.when('/patientDataPage', {
	templateUrl: '/patient_data.html',
	controller: 'patientDataCtrl'
   })
	.when('/insurancesPage', {
	templateUrl: '/insurances.html',
	controller: 'insurancesCtrl'
   })
	.when('/patientDetailsPage', {
	templateUrl: '/patient_details.html',
	controller: 'patientDetailsCtrl'
   })
	.when('/patientHistoryPage', {
	templateUrl: '/patient_history.html',
	controller: 'patientHistoryCtrl'
   })
	.when('/allergyHistoryPage', {
	templateUrl: '/allergy_history.html',
	controller: 'allergyHistoryCtrl'
   })
	.when('/chronicMedicationPage', {
	templateUrl: '/chronic_medication.html',
	controller: 'chronicMedicationCtrl'
   })
	.when('/physicalExaminationPage', {
	templateUrl: '/physical_examination.html',
	controller: 'physicalExaminationCtrl'
   })
	.when('/immunisationPage', {
	templateUrl: '/immunisation.html',
	controller: 'immunisationCtrl'
   })
	.when('/dataAnalyzingPage', {
	templateUrl: '/data_analyzing.html',
	controller: 'dataAnalyzingCtrl'
   })
	.when('/trackingBoardPage', {
	templateUrl: '/tracking_board.html',
	controller: 'trackingBoardCtrl'
   })
	.when('/labDataPage', {
	templateUrl: '/lab_data.html',
	controller: 'labDataCtrl'
   })
	.when('/radiologyPage', {
	templateUrl: '/radiology.html',
	controller: 'radiologyCtrl'
   })
	.when('/pathologyReportPage', {
	templateUrl: '/pathology_report.html',
	controller: 'pathologyReportCtrl'
   })
	.when('/imagesPage', {
	templateUrl: '/images.html',
	controller: 'ImagesCtrl'
   })
	.when('/textNotesPage', {
	templateUrl: '/text_notes.html',
	controller: 'textNotesCtrl'
   })
	.when('/soundsPage', {
	templateUrl: '/sounds.html',
	controller: 'soundsCtrl'
   })
	.when('/videosPage', {
	templateUrl: '/videos.html',
	controller: 'videosCtrl'
   })
	.when('/DrugModulePage', {
	templateUrl: '/prescription.html',
	controller: 'prescriptionCtrl'
   })
	.otherwise({
	redirectTo: '/index.html'
   });
}]);
