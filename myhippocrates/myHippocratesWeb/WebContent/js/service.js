'use strict';

var myHippocratesServices = angular.module('myHippocratesServices', ['ngResource']);

myHippocratesApp.factory('EntitlementService', function($window, $rootScope, $timeout) {
  var service = {};

  service.Login = function( role, username, password, callback ) {
	$timeout(function() {
		var response = { success: ( role === 'doctor' && username === 'Haus-Meister' && password === 'myhippocrates' ) || ( role === 'patient' && username === 'trevor.smith' && password === 'myhippocrates' ) };
		if (!response.success) {
			response.message = 'Username or password is not valid';
		}
		callback(response);
	}, 1000);
  };

  return {
    setCurrentPath: function(newPath) {
	$window.location.href = newPath;
	return this;
    },
    setCurrentRole: function(val) {
	$window.sessionStorage.setItem('currentRole', val);
        return this;
    },
    getCurrentRole: function() {
	return $window.sessionStorage.getItem('currentRole');
    },
    setCurrentUser: function(val) {
	$window.sessionStorage.setItem('currentUser', val);
        return this;
    },
    getCurrentUser: function() {
	return $window.sessionStorage.getItem('currentUser');
    },
  };
});


myHippocratesApp.factory('ScheduleLocalStorage', function($window, $rootScope) {
});

myHippocratesApp.factory('PrescriptionLocalStorage', function($window, $rootScope) {
  angular.element($window).on('storage', function(event) {
    if (event.key === 'my-storage') {
      $rootScope.$apply();
    }
  });
  return {
    getCurrentWindow: function() {
      return $window;
    },
    closeWindow: function() {
      $window.close();
    },
    reloadWindow: function() {
      $window.location.reload();
    },
    setParentWindow: function(val) {
      $window.sessionStorage.setItem('parentWindow', val);
      return this;
    },
    getParentWindow: function() {
      return $window.sessionStorage.getItem('parentWindow');
    },
    setCurrentModule: function(val) {
      $window.sessionStorage.setItem('currentModule', val);
      return this;
    },
    getCurrentModule: function() {
      return $window.sessionStorage.getItem('currentModule');
    },
    setTextContentAndClose: function(val) {
      window.localStorage && $window.localStorage.setItem('my-storage', val);
      //$window.sessionStorage && $window.sessionStorage.setItem('my-storage', val);
      $window.close();
      return this;
    },
    setTextContent: function(val) {
      $window.localStorage && $window.localStorage.setItem('my-storage', val);
      //$window.sessionStorage && $window.sessionStorage.setItem('my-storage', val);
      return this;
    },
    getTextContent: function() {
      return $window.localStorage && $window.localStorage.getItem('my-storage');
      //return $window.sessionStorage && $window.sessionStorage.getItem('my-storage');
    }
  };
});

myHippocratesApp.factory('PatientDetailsLocalStorage', function($window, $rootScope) {
  return {
    closeWindow: function() {
      $window.close();
    },
    setCompleteName: function(val) {
      $window.sessionStorage.setItem('completeName', val);
      return this;
    },
    getCompleteName: function() {
      return $window.sessionStorage.getItem('completeName');
    }
  };
});

myHippocratesApp.factory('PatientDataLocalStorage', function($window, $rootScope) {
  return {
	setFirstName: function(val) {
		$window.sessionStorage.setItem('first_name', val);
		return this;
   	},

	getFirstName: function() {
		return $window.sessionStorage.getItem('first_name');
   	},

	setLastName: function(val) {
		$window.sessionStorage.setItem('last_name', val);
		return this;
   	},

	getLastName: function() {
		return $window.sessionStorage.getItem('last_name');
   	},

	setBirthDate: function(val) {
		$window.sessionStorage.setItem('birth_date', val);
		return this;
   	},

	getBirthDate: function() {
		return $window.sessionStorage.getItem('birth_date');
   	},

	setSex: function(val) {
		$window.sessionStorage.setItem('sex', val);
		return this;
   	},

	getSex: function() {
		return $window.sessionStorage.getItem('sex');
   	},

	setSocialID: function(val) {
		$window.sessionStorage.setItem('socialID', val);
		return this;
   	},

	getSocialID: function() {
		return $window.sessionStorage.getItem('socialID');
   	},

	setPhoto: function(val) {
		$window.sessionStorage.setItem('photo', val);
		return this;
   	},

	getPhoto: function() {
		return $window.sessionStorage.getItem('photo');
   	},

	setHealthIns: function(val) {
		$window.sessionStorage.setItem('health_ins', val);
		return this;
   	},

	getHealthIns: function() {
		return $window.sessionStorage.getItem('health_ins');
   	},

	setComplementaryIns: function(val) {
		$window.sessionStorage.setItem('complementary_ins', val);
		return this;
   	},

	getComplementaryIns: function() {
		return $window.sessionStorage.getItem('complementary_ins');
   	},

	setAddress: function(val) {
		$window.sessionStorage.setItem('address', val);
		return this;
   	},

	getAddress: function() {
		return $window.sessionStorage.getItem('address');
   	},

	setPhone: function(val) {
		$window.sessionStorage.setItem('phone', val);
		return this;
   	},

	getPhone: function() {
		return $window.sessionStorage.getItem('phone');
   	},

	setMobile: function(val) {
		$window.sessionStorage.setItem('mobile', val);
		return this;
   	},

	getMobile: function() {
		return $window.sessionStorage.getItem('mobile');
   	},

	setICE: function(val) {
		$window.sessionStorage.setItem('ICE', val);
		return this;
   	},

	getICE: function() {
		return $window.sessionStorage.getItem('ICE');
   	},

	setEmail: function(val) {
		$window.sessionStorage.setItem('email', val);
		return this;
   	},

	getEmail: function() {
		return $window.sessionStorage.getItem('email');
   	},

	setLanguage: function(val) {
		$window.sessionStorage.setItem('language', val);
		return this;
   	},

	getLanguage: function() {
		return $window.sessionStorage.getItem('language');
   	},

	setMaritalStatus: function(val) {
		$window.sessionStorage.setItem('marital_status', val);
		return this;
   	},

	getMaritalStatus: function() {
		return $window.sessionStorage.getItem('marital_status');
   	},

	setChildreen: function(val) {
		$window.sessionStorage.setItem('childreen', val);
		return this;
   	},

	getChildreen: function() {
		return $window.sessionStorage.getItem('childreen');
   	},

	setDelivery: function(val) {
		$window.sessionStorage.setItem('delivery', val);
		return this;
   	},

	getDelivery: function() {
		return $window.sessionStorage.getItem('delivery');
   	},

	setNationality: function(val) {
		$window.sessionStorage.setItem('nationality', val);
		return this;
   	},

	getNationality: function() {
		return $window.sessionStorage.getItem('nationality');
   	},

	setOccupation: function(val) {
		$window.sessionStorage.setItem('occupation', val);
		return this;
   	},

	getOccupation: function() {
		return $window.sessionStorage.getItem('occupation');
   	},

	setHeight: function(val) {
		$window.sessionStorage.setItem('height', val);
		return this;
   	},

	getHeight: function() {
		return $window.sessionStorage.getItem('height');
   	},

	setWeight: function(val) {
		$window.sessionStorage.setItem('weight', val);
		return this;
   	},

	getWeight: function() {
		return $window.sessionStorage.getItem('weight');
   	},

	setBMI: function(val) {
		$window.sessionStorage.setItem('BMI', val);
		return this;
   	},

	getBMI: function() {
		return $window.sessionStorage.getItem('BMI');
   	},

	setONSET: function(val) {
		$window.sessionStorage.setItem('ONSET', val);
		return this;
   	},

	getONSET: function() {
		return $window.sessionStorage.getItem('ONSET');
   	},

	setSmoker: function(val) {
		$window.sessionStorage.setItem('smoker', val);
		return this;
   	},

	getSmoker: function() {
		return $window.sessionStorage.getItem('smoker');
   	},

	setPacksPY: function(val) {
		$window.sessionStorage.setItem('packspy', val);
		return this;
   	},

	getPacksPY: function() {
		return $window.sessionStorage.getItem('packspy');
   	}
  };
});
