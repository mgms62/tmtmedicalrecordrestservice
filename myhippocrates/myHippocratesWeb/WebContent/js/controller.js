'use strict';

var myHippocratesApp = angular.module('myHippocratesApp', []);

myHippocratesApp.controller('loginCtrl', [ '$scope', '$rootScope', 'EntitlementService', function($scope, $rootScope, EntitlementService) {
	this.loginIncorrect = false;

	this.User = ' ';

	this.Password = ' ';

	this.currentRole = ' ';

	this.incorrectLoginMessage = 'login incorrect';

	this.getCurrentRole = function() {
		return EntitlementService.getCurrentRole();
	};

	this.setCurrentRole = function(roleName) {
		return EntitlementService.setCurrentRole(roleName);
	};

	this.initCurrentRole = function() {
		this.currentRole = EntitlementService.getCurrentRole();
	}

	this.login = function() {
		if ( this.currentRole === 'doctor' || this.currentRole === 'staff' || this.currentRole === 'nursing' ) {
			if ( this.User === 'Haus-Meister' && this.Password === 'myhippocrates' ) {
				EntitlementService.setCurrentPath('./home.html');
				this.loginIncorrect = false;
			}
			else {
				this.loginIncorrect = true;
			}
		}
		if ( this.currentRole === 'patient' ) {
			if ( this.User === 'trevor.smith' && this.Password === 'myhippocrates' ) {
				EntitlementService.setCurrentPath('./home_patient.html');
				this.loginIncorrect = false;
			}
			else {
				this.loginIncorrect = true;
			}
		}
	};
}]);

myHippocratesApp.controller('homeCtrl', [ '$rootScope', 'EntitlementService', function($rootScope, MyHippocratesGlobalStorage) {
}]);

myHippocratesApp.controller('homePatientCtrl', [ '$rootScope', 'EntitlementService', function($rootScope, MyHippocratesGlobalStorage) {
}]);

myHippocratesApp.controller('patientListCtrl', [ '$scope', function($scope) {
}]);

myHippocratesApp.controller('aboutCtrl', [ '$scope', function($scope) {
}]);

myHippocratesApp.controller('scheduleCtrl', [ '$scope', function($scope) {
}]);

myHippocratesApp.controller('patientDetailsCtrl', [ '$scope', 'PatientDetailsLocalStorage', function($scope, PatientDetailsLocalStorage) {

	this.updateCompleteName = function(value) {
    		return PatientDetailsLocalStorage.setCompleteName(value);
	};

  	this.recoverCompleteName = function() {
    		return PatientDetailsLocalStorage.getCompleteName();
  	};
	
	this.fillFields = function() {
		this.completename = PatientDetailsLocalStorage.getCompleteName();
		if ( this.completename === 'johndoe' ) {
			this.patientDetails = {
				firstName: 'John',
				lastName: 'Doe',
				birthDate: '15/02/1963',
				sex: 'M',
				socialID: '1963021534530',
				photo: './people/johndoe.png',
				healthIns: ' ',
				complementaryIns: ' ',
				address: ' ',
				phone: ' ',
				mobile: ' ',
				ICE: ' ',
				email: ' ',
				language: ' ',
				maritalStatus: ' ',
				childreen: ' ',
				delivery: ' ',
				nationality: ' ',
				occupation: ' ',
				height: ' ',
				weight: ' ',
				BMI: ' ',
				ONSET: ' ',
				smoker: ' ',
				packsPY: ' '
			};
		}
		if ( this.completename === 'eleanorsmith' ) {
			this.patientDetails = {
				firstName: 'Eleanor',
				lastName: 'Smith',
				birthDate: '13/08/1934',
				sex: 'F',
				socialID: '1235445456511',
				photo: './people/EleanorSmith.png',
				healthIns: ' ',
				complementaryIns: ' ',
				address: '4 ribbon street SF',
				phone: '34345566',
				mobile: '66554433',
				ICE: ' ',
				email: 'esmith@gmail.com',
				language: 'english',
				maritalStatus: 'married',
				childreen: '1',
				delivery: ' ',
				nationality: 'US citizen',
				occupation: 'retired',
				height: '1m64',
				weight: '56kg',
				BMI: ' ',
				ONSET: ' ',
				smoker: 'no',
				packsPY: ' '
			};
		}
		if ( this.completename === 'paulsmith' ) {
			this.patientDetails = {
				firstName: 'Paul',
				lastName: 'Smith',
				birthDate: '23/02/1931',
				sex: 'M',
				socialID: '14345665456671',
				photo: './people/PaulSmith.png',
				healthIns: ' ',
				complementaryIns: ' ',
				address: '4 ribbon street SF',
				phone: '34345566',
				mobile: '654566',
				ICE: ' ',
				email: 'psmith@gmail.com',
				language: 'english',
				maritalStatus: 'married',
				childreen: '1',
				delivery: ' ',
				nationality: 'US citizen',
				occupation: 'retired',
				height: '1m78',
				weight: '70kg',
				BMI: ' ',
				ONSET: ' ',
				smoker: 'no',
				packsPY: ' '
			};
		}
		if ( this.completename === 'trevorsmith' ) {
			this.patientDetails = {
				firstName: 'Trevor',
				lastName: 'Smith',
				birthDate: '02/02/1971',
				sex: 'M',
				socialID: '1438756709',
				photo: './people/TrevorSmith.png',
				healthIns: ' ',
				complementaryIns: ' ',
				address: '4 Grovenor Avenue LA',
				phone: '324534',
				mobile: '6123234',
				ICE: ' ',
				email: 'trevorsmith@gmail.com',
				language: 'english',
				maritalStatus: 'married',
				childreen: '2',
				delivery: ' ',
				nationality: 'US citizen',
				occupation: 'banker',
				height: '1m82',
				weight: '79kg',
				BMI: ' ',
				ONSET: ' ',
				smoker: 'no',
				packsPY: ' '
			};
		}
		if ( this.completename === 'helensmith' ) {
			this.patientDetails = {
				firstName: 'Helen',
				lastName: 'Smith',
				birthDate: '09/04/1975',
				sex: 'F',
				socialID: '134242453',
				photo: './people/HelenSmith.png',
				healthIns: ' ',
				complementaryIns: ' ',
				address: '4 Grovenor Avenue LA',
				phone: '324534',
				mobile: '645456',
				ICE: ' ',
				email: 'shelen@gmail.com',
				language: 'english',
				maritalStatus: 'married',
				childreen: '2',
				delivery: ' ',
				nationality: 'US citizen',
				occupation: 'french teacher',
				height: '1m72',
				weight: '65kg',
				BMI: ' ',
				ONSET: ' ',
				smoker: 'no',
				packsPY: ' '
			};
		}
		if ( this.completename === 'ethansmith' ) {
			this.patientDetails = {
				firstName: 'Ethan',
				lastName: 'Smith',
				birthDate: '09/09/2003',
				sex: 'M',
				socialID: '1443554543',
				photo: './people/EthanSmith.png',
				healthIns: ' ',
				complementaryIns: ' ',
				address: '4 Grovenor Avenue LA',
				phone: '',
				mobile: '',
				ICE: ' ',
				email: '',
				language: 'english',
				maritalStatus: '',
				childreen: '0',
				delivery: ' ',
				nationality: 'US citizen',
				occupation: 'schooler',
				height: '1m46',
				weight: '52kg',
				BMI: ' ',
				ONSET: ' ',
				smoker: 'no',
				packsPY: ' '
			};
		}
		if ( this.completename === 'alexandrasmith' ) {
			this.patientDetails = {
				firstName: 'Alexandra',
				lastName: 'Smith',
				birthDate: '17/11/2002',
				sex: 'F',
				socialID: '143245345645',
				photo: './people/AlexandraSmith.png',
				healthIns: ' ',
				complementaryIns: ' ',
				address: '4 Grovenor Avenue LA',
				phone: '',
				mobile: '',
				ICE: ' ',
				email: '',
				language: 'english',
				maritalStatus: '',
				childreen: '0',
				delivery: ' ',
				nationality: 'US citizen',
				occupation: 'schooler',
				height: '1m51',
				weight: '50kg',
				BMI: ' ',
				ONSET: ' ',
				smoker: 'no',
				packsPY: ' '
			};
		}
		if ( this.completename === 'georgesduncan' ) {
			this.patientDetails = {
				firstName: 'Georges',
				lastName: 'Duncan',
				birthDate: '24/10/1932',
				sex: 'M',
				socialID: '14242424245',
				healthIns: ' ',
				photo: './people/GeorgesDuncan.png',
				complementaryIns: ' ',
				address: '12 Broadway NYC',
				phone: '35345676',
				mobile: '61243234',
				ICE: ' ',
				email: 'duncan.georges@yahoo.com',
				language: 'english',
				maritalStatus: 'married',
				childreen: '1',
				delivery: ' ',
				nationality: 'US citizen',
				occupation: 'retired',
				height: '1m79',
				weight: '76kg',
				BMI: ' ',
				ONSET: ' ',
				smoker: 'no',
				packsPY: ' '
			};
		}
		if ( this.completename === 'adrianaduncan' ) {
			this.patientDetails = {
				firstName: 'Adriana',
				lastName: 'Duncan',
				birthDate: '04/08/1935',
				sex: 'F',
				socialID: '1764553453',
				healthIns: ' ',
				photo: './people/AdrianaDuncan.png',
				complementaryIns: ' ',
				address: '12 Broadway NYC',
				phone: '35345645',
				mobile: '63435455',
				ICE: ' ',
				email: 'adrianaduncan@gmail.com',
				language: 'english',
				maritalStatus: 'married',
				childreen: '1',
				delivery: ' ',
				nationality: 'US citizen',
				occupation: 'retired',
				height: '1m74',
				weight: '68kg',
				BMI: ' ',
				ONSET: ' ',
				smoker: 'no',
				packsPY: ' '
			};
		}
	};
}]);

myHippocratesApp.controller('patientDataCtrl', [ '$scope', 'PatientDataLocalStorage', function($scope, PatientDataLocalStorage) {
}]);

myHippocratesApp.controller('chiefComplaintCtrl', [ '$scope', function($scope) {
}]);

myHippocratesApp.controller('physicalExaminationCtrl', [ '$scope', function($scope) {
	this.heart = {
		inspection: 'undefined',
		pmi: 'undefined',
		palpitation: 'undefined',
		auscultation: 'undefined'
	};

	this.switchHeartInspectionExamination = function(value) {
		this.heart.inspection = value;
	};

	this.switchHeartPMIExamination = function(value) {
		this.heart.pmi = value;
	};

	this.switchHeartPalpitationExamination = function(value) {
		this.heart.palpitation = value;
	};

	this.switchHeartAuscultationExamination = function(value) {
		this.heart.auscultation = value;
	};

	this.abdomen = {
		inspection: 'undefined',
		auscultation: 'undefined',
		percussion: 'undefined',
		palpitation: 'undefined',
		spleenPalpation: 'undefined',
		liverPalpation: 'undefined'
	};

	this.switchAbdomenInspectionExamination = function(value) {
		this.abdomen.inspection = value;
	};

	this.switchAbdomenAuscultationExamination = function(value) {
		this.abdomen.auscultation = value;
	};

	this.switchAbdomenPercussionExamination = function(value) {
		this.abdomen.percussion = value;
	};

	this.switchAbdomenPalpitationExamination = function(value) {
		this.abdomen.palpitation = value;
	};

	this.switchAbdomenSpleenPalpationExamination = function(value) {
		this.abdomen.spleenPalpation = value;
	};

	this.switchAbdomenLiverPalpationExamination = function(value) {
		this.abdomen.liverPalpation = value;
	};

	this.neck = {
		thyroid: 'undefined',
		trachea: 'undefined',
		carotid: 'undefined',
		jugularVeins: 'undefined',
		lymphNodes: 'undefined'
	};

	this.switchNeckThyroidExamination = function(value) {
		this.neck.thyroid = value;
	};

	this.switchNeckTracheaExamination = function(value) {
		this.neck.trachea = value;
	};

	this.switchNeckCarotidExamination = function(value) {
		this.neck.carotid = value;
	};

	this.switchNeckJugularVeinsExamination = function(value) {
		this.neck.jugularVeins = value;
	};

	this.switchNeckLymphNodesExamination = function(value) {
		this.neck.lymphNodes = value;
	};

	this.backandspine = {
		mobility: 'undefined',
		curvature: 'undefined',
		posture: 'undefined',
		tenderness: 'undefined',
		cvaTenderness: 'undefined',
	};

	this.switchBackandspineMobilityExamination = function(value) {
		this.backandspine.mobility = value;
	};

	this.switchBackandspineCurvatureExamination = function(value) {
		this.backandspine.curvature = value;
	};

	this.switchBackandspinePostureExamination = function(value) {
		this.backandspine.posture = value;
	};

	this.switchBackandspineTendernessExamination = function(value) {
		this.backandspine.tenderness = value;
	};

	this.switchBackandspineCVATendernessExamination = function(value) {
		this.backandspine.cvaTenderness = value;
	};

	this.chest = {
		wallShape: 'undefined',
		wallExpansion: 'undefined',
		accessoryMuscle: 'undefined',
		tactileFremitus: 'undefined',
		vocaleFremitus: 'undefined',
		percussion: 'undefined'
	};

	this.switchChestWallShapeExamination = function(value) {
		this.chest.wallShape = value;
	};

	this.switchChestWallExpansionExamination = function(value) {
		this.chest.wallExpansion = value;
	};

	this.switchChestAccessoryMuscleExamination = function(value) {
		this.chest.accessoryMuscle = value;
	};

	this.switchChestTactileFremitusExamination = function(value) {
		this.chest.tactileFremitus = value;
	};

	this.switchChestVocaleFremitusExamination = function(value) {
		this.chest.vocaleFremitus = value;
	};

	this.switchChestPercussionExamination = function(value) {
		this.chest.percussion = value;
	};

	this.ENT = {
		hearingAcuity: 'undefined',
		otoscopy: 'undefined',
		tongue: 'undefined',
		buccalMuccosa: 'undefined',
		teeth: 'undefined',
		gum: 'undefined'
	}

	this.switchENTHearingAcuityExamination = function(value) {
		this.ENT.hearingAcuity = value;
	};

	this.switchENTOtoscopyExamination = function(value) {
		this.ENT.otoscopy = value;
	};

	this.switchENTTongueExamination = function(value) {
		this.ENT.tongue = value;
	};

	this.switchENTBuccalMuccosaExamination = function(value) {
		this.ENT.buccalMuccosa = value;
	};

	this.switchENTTeethExamination = function(value) {
		this.ENT.teeth = value;
	};

	this.switchENTGumExamination = function(value) {
		this.ENT.gum = value;
	};
}]);

myHippocratesApp.controller('scheduleCtrl', [ '$scope', 'ScheduleLocalStorage', function($scope, ScheduleLocalStorage) {
}]);

myHippocratesApp.controller('prescriptionCtrl', [ '$scope', 'PrescriptionLocalStorage', function($scope, PrescriptionLocalStorage ) {

  this.value = PrescriptionLocalStorage.getTextContent();

  this.listOfDrugs = ' ';

  this.latestData = function() {
    return PrescriptionLocalStorage.getTextContent();
  };

  this.setParentWindow = function() {
    this.parentWindow = PrescriptionLocalStorage.getCurrentWindow();
    return PrescriptionLocalStorage.setParentWindow(parentWindow);
  };

  this.reloadParentWindow = function() {
    this.parentWindow = PrescriptionLocalStorage.getParentWindow();
    this.parentWindow.location.reload();
  };

  this.reset = function() {
    return PrescriptionLocalStorage.setTextContent(' ');
  };

  this.update = function(val) {
    return PrescriptionLocalStorage.setTextContent(val);
  };

 this.setCurrentModule = function(moduleName) {
    return PrescriptionLocalStorage.setCurrentModule(moduleName);
  };

  this.getCurrentModule = function() {
    return PrescriptionLocalStorage.getCurrentModule();
  };

    this.newTemplate = {
	shortcut: 'myShortcut ',
	description: 'myDescription',
	textContent: 'myText'
    };


    this.newPrescription = {
	editable: true,
	prescriptionDate: ' ',
	expirationDate: ' ',
	textContent: 'this is my new prescription!!!'
    };

    this.commercialDrugNames = ['AACIFEMINE (1*30 CPR.) 2MG', 'ALGIPAN (1*1 POMM. 80G)', 'AMBROHEXAL (1*1 GTTES 50ML) 7,5MG par 1ML', 'AMICLA (1*1 CR. 30G) 0,1PC', 'AMOXICILIN-RATIOPHARM-1000 (1*20 CPR.) 1000MG', 'AMOXI-HEXAL-750 (1*10 CPR.SS BLIST.) 750MG', 'ANDROCUR (3*15 CPR.) 10MG'];
    this.nonCommercialDrugNames = ['AACIFEMINE', 'ALGIPAN', 'AMBROHEXAL', 'AMICLA', 'AMOXICILIN-RATIOPHARM-1000', 'AMOXI-HEXAL-750', 'ANDROCUR'];

    this.drugItem = {};

    this.drugItem.commercialDrugName = 'AACIFEMINE (1*30 CPR.) 2MG';
    this.drugItem.nonCommercialDrugName = 'AACIFEMINE';

    this.amounts = ['1/2','1/4','1','2','3','4','5','6','7','8','9','10','15','20','30','50','100','200','500'];
    this.forms = ['amp.','applic.','bouff.','caps.','drag.'];
    this.frequencies = ['1x/day','2x/day','3x/day','4x/day'];
    this.times = ['morning','midday','evening'];
    this.meals = ['before','during','after'];
    this.dosage = {};
    this.dosage.amount = '1/2';
    this.dosage.form = 'amp.';
    this.dosage.frequency = '1x/day';
    this.dosage.time = 'morning';
    this.dosage.meal = 'before';

     this.appendDrugName = function() {
	this.value = this.value + ' ' + this.drugItem.commercialDrugName;
	this.listOfDrugs = this.listOfDrugs + this.drugItem.nonCommercialDrugName + '\n';
     };

     this.appendDosage = function() {
	this.value = this.value + ' ' + this.dosage.amount + ' ' + this.dosage.form + ' ' + this.dosage.frequency + ' ' + this.dosage.time + ' ' + this.dosage.meal;
     };

     this.confirmNewPrescriptionAndClose = function(aText) {
	return PrescriptionLocalStorage.setTextContentAndClose(aText) ;
     };

     this.closeWindow = function() {
	PrescriptionLocalStorage.closeWindow() ;
     };

     this.storeTemplate = function() {
	PrescriptionLocalStorage.closeWindow();
     };

     this.cancel = function() {
	PrescriptionLocalStorage.closeWindow();
     };

    this.modeEdition = true;
    this.testText = ' ';
    this.patientData = {
	firstName : 'John',
	lastName : 'Doe',
	birthDate : '15/02/1963',
	gender : 'male',
	socialID : '1963021545656'
    };
    this.currentPrescription = {
	editable: true,
	prescriptionDate: ' ',
	expirationDate: ' ',
	textContent: 'this is my prescription!!!'
    };

    this.getTextPrescription = function() {
	return(this.currentPrescription.textContent);
    }

    this.updateNewTemplate = function(aText) {
	return( PrescriptioLocalStorage.setTextContent(aText) );
    }
    this.desactivateMainPrescription = function() {
	this.currentPrescription.editable = false;
    };

    this.reactivateMainPrescription = function() {
	this.currentPrescription.editable = true;
    };

   this.templateGrantAccess = 'public';

   this.is_public = true;

    this.switchPrivilege = function() {
	if ( this.is_public ) {
		/*$scope.myTogglePrivateButtonStyle.border = '2px solid #40D5C5';
		$scope.myTogglePrivateButtonStyle.background-color = 'white';
		$scope.myTogglePrivateButtonStyle.color = '#40D5C5';
		$scope.myTogglePublicButtonStyle.border = '2px solid #40D5C5';
		$scope.myTogglePublicButtonStyle.background-color = '#40D5C5';
		$scope.myTogglePublicButtonStyle.color = '#white';*/
   		this.templateGrantAccess = 'private';
	}
	if ( !this.is_public ) {
		/*$scope.myTogglePublicButtonStyle.border = '2px solid #40D5C5';
		$scope.myTogglePublicButtonStyle.background-color = 'white';
		$scope.myTogglePublicButtonStyle.color = '#40D5C5';
		$scope.myTogglePrivateButtonStyle.border = '2px solid #40D5C5';
		$scope.myTogglePrivateButtonStyle.background-color = '#40D5C5';
		$scope.myTogglePrivateButtonStyle.color = '#white';*/
   		this.templateGrantAccess = 'public';
	}
	this.is_public = !this.is_public;
     };
}]);
