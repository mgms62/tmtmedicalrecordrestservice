/**
 * 
 */
package eu.myhippocrates.database;

/**
 * @author Gilles MARIA SUBE
 *
 */

import	java.io.Serializable;

import	javax.persistence.Column;
import	javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import	javax.xml.bind.annotation.XmlAccessorType;
import	javax.xml.bind.annotation.XmlRootElement;
import	javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {})
@XmlRootElement(name = "country")
@Entity @Table(name = "country")
@NamedQuery(name="Country.findAll", query="SELECT c FROM Country c")

public class Country implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public Country() {
		// TODO Auto-generated constructor stub
	}
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="country_id", nullable=false)
	private	int		country_id;
	@Column(name="code")
	private	String	code;
	@Column(name="name")
	private	String	name;
	@Column(name="postal_code")
	private	String	postal_code;
	@Column(name="iso3166_alpha3")
	private	String	iso3166_alpha3;
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public int getCountry_id() {
		return country_id;
	}
	
	public String getCode() {
		return code;
	}
	
	public String getIso3166_alpha3() {
		return iso3166_alpha3;
	}

	public String getName() {
		return name;
	}
	
	public String getPostal_code() {
		return postal_code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public void setCountry_id(int country_id) {
		this.country_id = country_id;
	}
	
	public void setIso3166_alpha3(String iso3166_alpha3) {
		this.iso3166_alpha3 = iso3166_alpha3;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setPostal_code(String postal_code) {
		this.postal_code = postal_code;
	}
}
