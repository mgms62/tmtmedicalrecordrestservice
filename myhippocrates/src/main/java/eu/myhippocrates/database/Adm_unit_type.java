/**
 * 
 */
package eu.myhippocrates.database;

/**
 * @author Gilles MARIA SUBE
 *
 */

import	java.io.Serializable;
import	javax.persistence.*;

import	javax.persistence.Column;
import	javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import	javax.xml.bind.annotation.XmlAccessorType;
import	javax.xml.bind.annotation.XmlRootElement;
import	javax.xml.bind.annotation.XmlType;


import	java.io.Serializable;

import	javax.persistence.Column;
import	javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import	javax.xml.bind.annotation.XmlAccessorType;
import	javax.xml.bind.annotation.XmlRootElement;
import	javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {})
@XmlRootElement(name = "adm_unit_type")
@Entity @Table(name = "adm_unit_type")
@NamedQuery(name="Adm_unit_type.findAll", query="SELECT aut FROM Adm_unit_type aut")

public class Adm_unit_type implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="adm_unit_type_id", nullable=false)
	private	int		adm_unit_type_id;
	@Column(name="country_id", nullable=false)
	private	int		country_id;
	@Column(name="name")
	private	String	name;
	
	private	Country	country;


	public Adm_unit_type() {
		// TODO Auto-generated constructor stub
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public int getAdm_unit_type_id() {
		return adm_unit_type_id;
	}
	
	public int getCountry_id() {
		return country_id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setAdm_unit_type_id(int adm_unit_type_id) {
		this.adm_unit_type_id = adm_unit_type_id;
	}
	
	public void setCountry_id(int country_id) {
		this.country_id = country_id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@OneToOne(mappedBy="Country")
	public Country getCountry() {
		return country;
	}
	
	public void setCountry(Country country) {
		this.country = country;
	}
}
