/**
 * 
 */
package eu.myhippocrates.database;

/**
 * @author Gilles MARIA SUBE
 *
 */

import	java.io.Serializable;

public class User implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private	long	user_id;
	private	String	login;
	private	String	name;
	
	public User() {
		// TODO Auto-generated constructor stub
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public long getUser_id() {
		return user_id;
	}
	
	public String getLogin() {
		return login;
	}
	
	public String getName() {
		return name;
	}
	
	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public void setName(String name) {
		this.name = name;
	}

}
