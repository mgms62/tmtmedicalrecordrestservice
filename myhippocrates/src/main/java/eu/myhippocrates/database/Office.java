/**
 * 
 */
package eu.myhippocrates.database;

/**
 * @author Gilles MARIA SUBE
 *
 */

import	java.io.Serializable;

import javax.persistence.Column;
import	javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import	javax.persistence.Table;
import	javax.persistence.NamedQuery;

import	javax.xml.bind.annotation.XmlAccessType;
import	javax.xml.bind.annotation.XmlAccessorType;
import	javax.xml.bind.annotation.XmlAttribute;
import	javax.xml.bind.annotation.XmlElement;
import	javax.xml.bind.annotation.XmlRootElement;
import	javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = " ", propOrder = {"office_id", "name", "website", "email", "information", "office_number_id"})
@XmlRootElement(name = "office")
@Entity @Table(name = "office")
@NamedQuery(name="Office.findAll", query="SELECT o FROM Office o")


public class Office implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="office_id", nullable=false)
	private long 	office_id;
	@Column(name="site_id")
	private	long	site_id;
	@Column(name="name")
	private String 	name;
	@Column(name="website")
	private String 	website;
	@Column(name="email")
	private String 	email;
	@Column(name="information")
	private String 	information;
	@Column(name="office_number_id")
	private String 	office_number_id;

	public Office() {
	
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

		public	long	getOffice_id() {
			return( this.office_id );
		}

		public void	setOffice_id( long office_id ) {
			this.office_id = office_id;
		}

		public String	getName() {
			return( this.name );
		}

		public void	setName( String name ) {
			this.name = name;
		}

		public	String	getWebsite() {
			return( this.website );
		}

		public void	setWebsite( String website ) {
			this.website = website;
		}

		public	String	getEmail() {
			return( this.email );
		}

		public void	setEmail( String email ) {
			this.email = email;
		}

		public	String	getInformation() {
			return( this.information );
		}

		public void	setInformation( String information ) {
			this.information = information;
		}

		public	String	getOffice_number_id() {
			return( this.office_number_id );
		}
		
	public void setOffice_number_id(String office_number_id) {
		this.office_number_id = office_number_id;
	}	
		
		public long getSite_id() {
			return site_id;
		}

		public void setSite_id(long site_id) {
			this.site_id = site_id;
		}
		
}
