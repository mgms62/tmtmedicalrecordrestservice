/**
 * 
 */
package eu.myhippocrates.database;

/**
 * @author Gilles MARIA SUBE
 *
 */

import	java.io.Serializable;

import	javax.persistence.Column;
import	javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import	javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlAccessType;
import	javax.xml.bind.annotation.XmlAccessorType;
import	javax.xml.bind.annotation.XmlRootElement;
import	javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {})
@XmlRootElement(name = "zip")
@Entity @Table(name = "zip")
@NamedQuery(name="Zip.findAll", query="SELECT z FROM Zip z")

public class Zip implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="zip_id", nullable=false)
	private	long	zip_id;
	@Column(name="zip")
	private	int		zip;
	@Column(name="locality_id")
	private	long	locality_id;
	@Column(name="street")
	private	String	street;
	@Column(name="parity")
	private	char    parity;
	@Column(name="first")
	private	int		first;
	@Column(name="last")
	private	int		last;
	
	private	Municipality municipality;
	
	public Zip() {
		// TODO Auto-generated constructor stub
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public int getFirst() {
		return first;
	}
	
	public int getLast() {
		return last;
	}
	
	public long getLocality_id() {
		return locality_id;
	}
	
	public char getParity() {
		return parity;
	}
	
	public String getStreet() {
		return street;
	}
	
	public int getZip() {
		return zip;
	}
	
	public long getZip_id() {
		return zip_id;
	}
	
	public void setFirst(int first) {
		this.first = first;
	}
	
	public void setLast(int last) {
		this.last = last;
	}
	
	public void setLocality_id(long locality_id) {
		this.locality_id = locality_id;
	}
	
	public void setParity(char parity) {
		this.parity = parity;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	public void setZip(int zip) {
		this.zip = zip;
	}
	
	public void setZip_id(long zip_id) {
		this.zip_id = zip_id;
	}
	
	@OneToOne(mappedBy="Municipality")
	public Municipality getMunicipality() {
		return municipality;
	}
	
	public void setMunicipality(Municipality municipality) {
		this.municipality = municipality;
	}

}
