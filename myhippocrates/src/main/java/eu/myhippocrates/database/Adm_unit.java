/**
 * 
 */
package eu.myhippocrates.database;

/**
 * @author Gilles MARIA SUBE
 *
 */

import	java.io.Serializable;

import javax.persistence.CascadeType;
import	javax.persistence.Column;
import	javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import	javax.xml.bind.annotation.XmlAccessorType;
import	javax.xml.bind.annotation.XmlRootElement;
import	javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {})
@XmlRootElement(name = "adm_unit")
@Entity @Table(name = "adm_unit")
@NamedQuery(name="Adm_unit.findByName", query="")


public class Adm_unit implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private	Country	country;
	
	public Adm_unit() {
		// TODO Auto-generated constructor stub
	}
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="adm_unit_id", nullable=false)
	private	long	adm_unit_id;
	@Column(name="country_id", nullable=false)
	private	int		country_id;
	@Column(name="adm_unit_type_id", nullable=false)
	private	int		adm_unit_type_id;
	@Column(name="code", nullable=false)
	private	String	code;
	@Column(name="official_name")
	private	String	official_name;
	@Column(name="local_name", nullable=false)
	private	String	local_name;
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public long getAdm_unit_id() {
		return adm_unit_id;
	}
	
	public int getAdm_unit_type_id() {
		return adm_unit_type_id;
	}
	
	public String getCode() {
		return code;
	}
	
	public int getCountry_id() {
		return country_id;
	}
	
	public String getLocal_name() {
		return local_name;
	}
	
	public String getOfficial_name() {
		return official_name;
	}
	
	//one to one relation (FK)
	@OneToOne(mappedBy="Country")
	public Country getCountry() {
		return country;
	}
	
	public void setAdm_unit_id(long adm_unit_id) {
		this.adm_unit_id = adm_unit_id;
	}
	
	public void setAdm_unit_type_id(int adm_unit_type_id) {
		this.adm_unit_type_id = adm_unit_type_id;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public void setCountry_id(int country_id) {
		this.country_id = country_id;
	}
	
	public void setLocal_name(String local_name) {
		this.local_name = local_name;
	}
	
	public void setOfficial_name(String official_name) {
		this.official_name = official_name;
	}
	
	public void setCountry(Country country) {
		this.country = country;
	}
}
