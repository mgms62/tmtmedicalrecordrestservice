/**
 * 
 */
package eu.myhippocrates.database;

/**
 * @author Gilles MARIA SUBE
 *
 */

import	java.io.Serializable;

import	javax.persistence.Column;
import	javax.persistence.Entity;
import	javax.persistence.ManyToOne;
import	javax.persistence.JoinColumn;
import	javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import	javax.xml.bind.annotation.XmlAccessorType;
import	javax.xml.bind.annotation.XmlRootElement;
import	javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {})
@XmlRootElement(name = "site")
@Entity @Table(name = "site")
@NamedQuery(name="Site.findAll", query="SELECT s FROM Site s")

public class Site implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="site_id", nullable=false)
	private	long	site_id;
	@Column(name="office_id")
	private	long	office_id;
	@Column(name="name")
	private	String	name;
	@Column(name="website")
	private	String	website;
	@Column(name="email")
	private	String	email;
	@Column(name="phone_number")
	private	String	phone_number;
	@Column(name="fax_number")
	private	String	fax_number;
	@Column(name="information")
	private	String	information;
	@Column(name="unit_code")
	private	String	unit_code;
	@Column(name="address_id")
	private	long	address_id;
	
	private	Office	office;
	
	public Site() {
		// TODO Auto-generated constructor stub
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public long getSite_id() {
		return site_id;
	}
	
	public long getOffice_id() {
		return office_id;
	}
	
	public long getAddress_id() {
		return address_id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getWebsite() {
		return website;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getPhone_number() {
		return phone_number;
	}
	
	public String getFax_number() {
		return fax_number;
	}
	
	public String getInformation() {
		return information;
	}
	
	public String getUnit_code() {
		return unit_code;
	}
	
	public void setSite_id(long site_id) {
		this.site_id = site_id;
	}
	
	public void setOffice_id(long office_id) {
		this.office_id = office_id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}
	
	public void setFax_number(String fax_number) {
		this.fax_number = fax_number;
	}
	
	public void setWebsite(String website) {
		this.website = website;
	}
	
	public void setUnit_code(String unit_code) {
		this.unit_code = unit_code;
	}
	
	public void setInformation(String information) {
		this.information = information;
	}
	
    public void setAddress_id(long address_id) {
	this.address_id = address_id;
   }

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="office_id", nullable=false)
    public Office getOffice() {
		return office;
	}

	public void setOffice(Office office) {
		this.office = office;
	}
}
