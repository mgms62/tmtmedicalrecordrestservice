/**
 * 
 */
package eu.myhippocrates.database;

/**
 * @author Gilles MARIA SUBE
 *
 */

import	java.io.Serializable;

import	javax.persistence.Column;
import	javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import	javax.xml.bind.annotation.XmlAccessorType;
import	javax.xml.bind.annotation.XmlRootElement;
import	javax.xml.bind.annotation.XmlType;

import eu.myhippocrates.database.Municipality;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {})
@XmlRootElement(name = "municipality")
@Entity @Table(name = "municipality")
@NamedQuery(name="Municipality.findByName", query="SELECT m FROM Municipality m")

public class Municipality implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public Municipality() {
		// TODO Auto-generated constructor stub
	}
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="municipality_id", nullable=false)
	private	long	municipality_id;
	@Column(name="country_id")
	private	int		country_id;
	@Column(name="adm_unit_id")
	private	int		adm_unit_id;
	@Column(name="official_name")
	private	String	official_name;
	@Column(name="local_name")
	private	String	local_name;
	@Column(name="zip_id")
	private	long	zip_id;
	
	private	Country			country;
	private	Zip				zip;
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public int getAdm_unit_id() {
		return adm_unit_id;
	}
	
	public int getCountry_id() {
		return country_id;
	}
	
	public String getLocal_name() {
		return local_name;
	}
	
	public long getMunicipality_id() {
		return municipality_id;
	}
	
	public String getOfficial_name() {
		return official_name;
	}
	
	public long getZip_id() {
		return zip_id;
	}
	
	public void setAdm_unit_id(int adm_unit_id) {
		this.adm_unit_id = adm_unit_id;
	}
	
	public void setCountry_id(int country_id) {
		this.country_id = country_id;
	}
	
	public void setLocal_name(String local_name) {
		this.local_name = local_name;
	}
	
	public void setMunicipality_id(long municipality_id) {
		this.municipality_id = municipality_id;
	}
	
	public void setOfficial_name(String official_name) {
		this.official_name = official_name;
	}
	
	public void setZip_id(long zip_id) {
		this.zip_id = zip_id;
	}
	
	@OneToOne(mappedBy="Country")
	public Country getCountry() {
		return country;
	}
	
	@OneToOne(mappedBy="Zip")
	public Zip getZip() {
		return zip;
	}
	
	public void setCountry(Country country) {
		this.country = country;
	}
	
	public void setZip(Zip zip) {
		this.zip = zip;
	}
	
}
