/**
 * 
 */
package eu.myhippocrates.database;

/**
 * @author Gilles MARIA SUBE
 *
 */

import	java.io.Serializable;

import javax.persistence.CascadeType;
import	javax.persistence.Column;
import	javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import	javax.xml.bind.annotation.XmlAccessorType;
import	javax.xml.bind.annotation.XmlRootElement;
import	javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {})
@XmlRootElement(name = "locality")
@Entity @Table(name = "locality")
@NamedQuery(name="Locality.findByName", query="")

public class Locality  implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public Locality() {
		// TODO Auto-generated constructor stub
	}
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="locality_id", nullable=false)
	private	long	locality_id;
	@Column(name="country_id", nullable=false)
	private	int		country_id;
	@Column(name="municipality_id")
	private	long	municipality_id;
	@Column(name="official_name")
	private	String	official_name;
	@Column(name="local_name")
	private	String	local_name;
	@Column(name="zip_id")
	private	long	zip_id;
	
	private	Country			country;
	private	Municipality	municipality;
	private	Zip				zip;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public long getLocality_id() {
		return locality_id;
	}
	
	public int getCountry_id() {
		return country_id;
	}
	
	public String getLocal_name() {
		return local_name;
	}
	
	public long getMunicipality_id() {
		return municipality_id;
	}
	
	public String getOfficial_name() {
		return official_name;
	}
	
	public long getZip_id() {
		return zip_id;
	}
	
	public void setLocality_id(int locality_id) {
		this.locality_id = locality_id;
	}
	
	public void setCountry_id(int country_id) {
		this.country_id = country_id;
	}
	
	public void setLocal_name(String local_name) {
		this.local_name = local_name;
	}
	
	public void setMunicipality_id(long municipality_id) {
		this.municipality_id = municipality_id;
	}
	
	public void setOfficial_name(String official_name) {
		this.official_name = official_name;
	}
	
	public void setZip_id(long zip_id) {
		this.zip_id = zip_id;
	}
	
	@OneToOne(mappedBy="Municipality")
	public Municipality getMunicipality() {
		return municipality;
	}
	
	@OneToOne(mappedBy="Country")
	public Country getCountry() {
		return country;
	}
	
	@OneToOne(mappedBy="Zip")
	public Zip getZip() {
		return zip;
	}
	
	public void setCountry(Country country) {
		this.country = country;
	}
	
	public void setMunicipality(Municipality municipality) {
		this.municipality = municipality;
	}
	
	public void setZip(Zip zip) {
		this.zip = zip;
	}
}
