/**
 * 
 */
package eu.myhippocrates.database;

/**
 * @author Gilles MARIA SUBE
 *
 */

import	java.io.Serializable;

import	javax.persistence.Column;
import	javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import	javax.xml.bind.annotation.XmlAccessorType;
import	javax.xml.bind.annotation.XmlRootElement;
import	javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"address_id","address_type"})
@XmlRootElement(name = "address")
@Entity @Table(name = "address")
@NamedQuery(name="Address.findAll", query="SELECT a FROM Address")

public class Address implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="address_id", nullable=false)
	private	long	address_id;
	@Column(name="address_type", nullable=false)
	private	String	address_type;
	@Column(name="street_name")
	private	String	street_name;
	@Column(name="street_number")
	private	String	street_number;
	@Column(name="postal_code")
	private	String	postal_code;
	@Column(name="postal_box")
	private	String	postal_box;
	@Column(name="code_box")
	private	String	code_box;
	@Column(name="locality")
	private	String	locality;
	@Column(name="country")
	private	String	country;

	
	public Address() {
		// TODO Auto-generated constructor stub
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public long getAddress_id() {
		return address_id;
	}
	
	public String getAddress_type() {
		return address_type;
	}
	
	public String getCode_box() {
		return code_box;
	}
	
	public String getCountry() {
		return country;
	}
	
	public String getLocality() {
		return locality;
	}
	
	public String getPostal_box() {
		return postal_box;
	}
	
	public String getPostal_code() {
		return postal_code;
	}
	
	public String getStreet_name() {
		return street_name;
	}
	
	public String getStreet_number() {
		return street_number;
	}
	
	public void setAddress_id(long address_id) {
		this.address_id = address_id;
	}
	
	public void setAddress_type(String address_type) {
		this.address_type = address_type;
	}
	
	public void setCode_box(String code_box) {
		this.code_box = code_box;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public void setLocality(String locality) {
		this.locality = locality;
	}
	
	public void setPostal_box(String postal_box) {
		this.postal_box = postal_box;
	}
	
	public void setPostal_code(String postal_code) {
		this.postal_code = postal_code;
	}
	
	public void setStreet_name(String street_name) {
		this.street_name = street_name;
	}
	
	public void setStreet_number(String street_number) {
		this.street_number = street_number;
	}
	
}
