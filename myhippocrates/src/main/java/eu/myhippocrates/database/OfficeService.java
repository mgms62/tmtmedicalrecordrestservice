/**
 * 
 */
package eu.myhippocrates.database;


/**
 * @author Gilles MARIA SUBE
 *
 */

import	java.util.List;
import	java.net.URI;

import	org.apache.logging.log4j.*;

import	javax.persistence.EntityManager;
import	javax.persistence.EntityManagerFactory;
import	javax.persistence.EntityTransaction;
import	javax.persistence.Query;
import	javax.persistence.Column;
import	javax.persistence.Entity;
import	javax.persistence.TypedQuery;
import	javax.persistence.NamedQuery;
import	javax.persistence.NamedQueries;
import	javax.persistence.Persistence;
import	javax.persistence.Table;

import	javax.ws.rs.Consumes;
import	javax.ws.rs.DELETE;
import	javax.ws.rs.GET;
import	javax.ws.rs.POST;
import	javax.ws.rs.PUT;
import	javax.ws.rs.Path;
import	javax.ws.rs.PathParam;
import	javax.ws.rs.Produces;
import	javax.ws.rs.core.Response;

@Entity
@Table(name="office")
@NamedQueries({
	@NamedQuery(name="Office.findAll", query="SELECT o from Office o"),
	@NamedQuery(name="Office.findByID", query="SELECT o from Office o WHERE office_id=:office_id"),
	@NamedQuery(name="Office.createRecord", query="INSERT INTO Office o WHERE office_id=:office_id"),
	@NamedQuery(name="Office.updateRecord", query="UPDATE Office SET office_id=:office_id, ")
})
public class OfficeService {
	long	office_id;
	
	static Logger	logger = LogManager.getLogger();

	public	OfficeService( long id ) {
		this.office_id = id;
	}

	@GET
	@Produces("application/json")

	public List<Office>	getOffices() {
		EntityManagerFactory	emf = Persistence.createEntityManagerFactory("");
		EntityManager		em = emf.createEntityManager();
		TypedQuery<Office>	query = em.createNamedQuery("Office.findAll", Office.class);
		List<Office>		offices = query.getResultList();

		return(offices);
	}

	public Office	getOffice(@PathParam("office_id") long office_id) {
		Office			off = null;
		EntityManagerFactory	emf = Persistence.createEntityManagerFactory("");
		EntityManager		em = emf.createEntityManager();
		TypedQuery<Office>			query = em.createNamedQuery("Office.findByID",Office.class);
					//off = query.getResultList();

		return( off );
	}

	@POST
	@Consumes("application/json")
	public	Response	insertOffice( Office o ) {
		Response		response = null;
		EntityManagerFactory	emf = Persistence.createEntityManagerFactory("");
		EntityManager		em = emf.createEntityManager();
		Query			query = em.createNamedQuery("Office.createRecord");
		EntityTransaction	tx = em.getTransaction();
					tx.begin();
					em.persist(o);
					tx.commit();
					em.close();
					emf.close();

		return( Response.created(URI.create("officeId:" + o.getOffice_id())) ).build();
	}

	@PUT
	@Consumes("application/json")
	public	Response	updateOffice( Office o ) {
		Response		response = null;
		EntityManagerFactory	emf = Persistence.createEntityManagerFactory("");
		EntityManager		em = emf.createEntityManager();
		Query			query = em.createNamedQuery("Office.updateRecord");
		EntityTransaction	tx = em.getTransaction();
					tx.begin();
					em.persist(o);
					tx.commit();
					em.close();
					emf.close();

		return( Response.created(URI.create("officeId:" + o.getOffice_id()))).build();
	}

}
