package eu.myhippocrates.drug.module;

import javax.ws.rs.POST;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * Hello world!
 *
 */

@XmlAccessorType
public class App 
{
	@POST
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
}
