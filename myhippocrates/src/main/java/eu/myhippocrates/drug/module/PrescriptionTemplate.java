/**
 * 
 */
package eu.myhippocrates.drug.module;

/**
 * @author Gilles MARIA SUBE
 *
 */

import	java.io.Serializable;

import javax.persistence.CascadeType;
import	javax.persistence.Column;
import	javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import	javax.xml.bind.annotation.XmlAccessorType;
import	javax.xml.bind.annotation.XmlRootElement;
import	javax.xml.bind.annotation.XmlType;

import eu.myhippocrates.database.User;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {""})
@XmlRootElement(name = "prescriptiontemplate")
@Entity @Table(name = "prescriptiontemplate")
@NamedQuery(name="PrescriptionTemplate.findAll", query="SELECT prtmp FROM PrescriptionTemplate prtmp")

public class PrescriptionTemplate implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="prescriptiontemplate_id", nullable=false)
	private	long	prescriptiontemplate_id;
	@Column(name="prescription_type_id", nullable=false)
	private	long	prescription_type_id;
	@Column(name="shortname")
	private	String	shortname;
	@Column(name="longname")
	private	String	longname;
	@Column(name="text_content")
	private	String	text_content;
	@Column(name="is_public")
	private	boolean	is_public;
	@Column(name="owner_id", nullable=false)
	private	long	owner_id;
	@Column(name="physician_id")
	private	long	physician_id;
	@Column(name="date_creation", nullable=false)
	private	String	date_creation;
	@Column(name="date_modification")
	private	String	date_modification;
	
	@ManyToOne
	@JoinColumn(name="user_id", nullable=false, insertable=false, updatable=false)
	private	User				user;
	
	@ManyToOne
	@JoinColumn(name="prescription_type_id", nullable=false, insertable=false, updatable=false)
	private Prescription_type	prescription_type;
	
	public PrescriptionTemplate() {
		// TODO Auto-generated constructor stub
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public long getPrescriptiontemplate_id() {
		return prescriptiontemplate_id;
	}
	
	public long getPrescription_type_id() {
		return prescription_type_id;
	}
	
	public String getShortname() {
		return shortname;
	}
	
	public String getLongname() {
		return longname;
	}
	
	public String getText_content() {
		return text_content;
	}
	
	public boolean isIs_public() {
		return is_public;
	}
	
	public long getOwner_id() {
		return owner_id;
	}
	
	public String getDate_creation() {
		return date_creation;
	}
	
	public long getPhysician_id() {
		return physician_id;
	}
	
	public String getDate_modification() {
		return date_modification;
	}
	
	public void setPrescriptiontemplate_id(long prescriptiontemplate_id) {
		this.prescriptiontemplate_id = prescriptiontemplate_id;
	}
	
	public void setPrescription_type_id(long prescription_type_id) {
		this.prescription_type_id = prescription_type_id;
	}
	
	public void setShortname(String shortname) {
		this.shortname = shortname;
	}
	
	public void setLongname(String longname) {
		this.longname = longname;
	}
	
	public void setText_content(String text_content) {
		this.text_content = text_content;
	}
	
	public void setIs_public(boolean is_public) {
		this.is_public = is_public;
	}
	
	public void setOwner_id(long owner_id) {
		this.owner_id = owner_id;
	}
	
	public void setDate_creation(String date_creation) {
		this.date_creation = date_creation;
	}
	
	public void setPhysician_id(long physician_id) {
		this.physician_id = physician_id;
	}
	
	public void setDate_modification(String date_modification) {
		this.date_modification = date_modification;
	}
	
	public User getUser() {
		return user;
	}
	
	public Prescription_type getPrescription_type() {
		return prescription_type;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public void setPrescription_type(Prescription_type prescription_type) {
		this.prescription_type = prescription_type;
	}

}
