/**
 * 
 */
package eu.myhippocrates.drug.module;

/**
 * @author Gilles MARIA SUBE
 *
 */

import	java.io.Serializable;
import	javax.persistence.*;

import	javax.persistence.Column;
import	javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import	javax.xml.bind.annotation.XmlAccessorType;
import	javax.xml.bind.annotation.XmlRootElement;
import	javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {})
@XmlRootElement(name = "prescription_type")
@Entity @Table(name = "prescription_type")
@NamedQuery(name="Prescription_type.findAll", query="SELECT pt FROM Prescription_type pt")

public class Prescription_type implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue
	@Column(name="prescription_type_id", nullable=false)
	private	int		prescription_type_id;
	@Column(name="name")
	private	String	name;
	
	public Prescription_type() {
		// TODO Auto-generated constructor stub
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public int getPrescription_type_id() {
		return prescription_type_id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setPrescription_type_id(int prescription_type_id) {
		this.prescription_type_id = prescription_type_id;
	}
	
	public void setName(String name) {
		this.name = name;
	}

}
