/**
 * 
 */
package eu.myhippocrates.drug.module;

/**
 * @author Gilles MARIA SUBE
 *
 */

import	java.io.Serializable;
import	javax.persistence.*;

import javax.xml.bind.annotation.XmlAccessType;
import	javax.xml.bind.annotation.XmlAccessorType;
import	javax.xml.bind.annotation.XmlRootElement;
import	javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonInclude;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {})
@XmlRootElement(name = "prescriptions")
@Entity @Table(name = "prescriptions")
@NamedQuery(name="Prescriptions.findForPatient", query="SELECT p FROM Prescriptions p where p.patientData.patientId = :patientId")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Prescriptions implements Serializable {
	
private static final long serialVersionUID = 1L;

	//entities binded through FK

	@ManyToOne
	@JoinColumn(name="physician_id", nullable=false, insertable=false, updatable=false)
	private	Physician			physician; 
	
	@ManyToOne
	@JoinColumn(name="patientId", nullable=false, insertable=false, updatable=false)
	private	PatientData			patientData; 
	
	@ManyToOne
	@JoinColumn(name="prescription_type_id", nullable=false, insertable=false, updatable=false)
	private	Prescription_type	prescription_type;
	
	private	Incident			incident;
	
	@Id @GeneratedValue
	@Column(name="prescription_id", nullable=false)
	private long 	prescription_id;
	@Column(name="incident_id", nullable=false)
	private int 	incident_id;
	@Column(name="creation_date", nullable=false)
	private String 	creationDate;
	@Column(name="created_by")
	private String 	createdBy;
	@Column(name="physician_id", nullable=false)
	private long 	physician_id;
	@Column(name="prescription_type_id")
	private int 	prescription_type_id;
	@Column(name="prescription_date")
	private String 	prescriptionDate;
	@Column(name="expiry_date")
	private String 	expiryDate;
	@Column(name="accident_number")
	private String 	accidentNumber;
	@Column(name="accident_date")
	private String 	accidentDate;
	@Column(name="patientId", nullable=false)
	private	long	patientId;
	@Column(name="patient_name")
	private String 	patientName;
	@Column(name="patient_first_name")
	private String 	patientfirstName;
	@Column(name="patient_maiden_name")
	private String 	patientMaidenName;
	@Column(name="patient_address")
	private String 	patient_address;
	@Column(name="social_id", nullable=false)
	private	String	social_id;
	@Column(name="professional_physician_number_id", nullable=false)
	private String	professional_physician_number_id;
	@Column(name="billing_physician_number_id")
	private	String	billing_physician_number_id;
	@Column(name="physician_full_name")
	private	String	physicianFullName;
	@Column(name="physician_speciality")
	private	String	physicianSpeciality;
	@Column(name="physician_address")
	private	String	physicianAddress;
	@Lob @Column(name="text_content")
	private	String	textContent;

	public Prescriptions() {
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public long getPrescription_id() {
		return prescription_id;
	}
	
	public String getAccidentDate() {
		return accidentDate;
	}
	
	public String getAccidentNumber() {
		return accidentNumber;
	}
	
	public String getBilling_physician_number_id() {
		return billing_physician_number_id;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}
	
	public String getCreationDate() {
		return creationDate;
	}
	
	public String getExpiryDate() {
		return expiryDate;
	}
	
	public int getIncident_id() {
		return incident_id;
	}
	
	public String getSocial_id() {
		return social_id;
	}
	
	public long getPatientId() {
		return patientId;
	}
	
	public String getPatientfirstName() {
		return patientfirstName;
	}
	
	public String getPatientMaidenName() {
		return patientMaidenName;
	}
	
	public String getPatientName() {
		return patientName;
	}
	
	public long getPhysician_id() {
		return physician_id;
	}
	
	public String getPhysicianFullName() {
		return physicianFullName;
	}
	
	public String getPhysicianSpeciality() {
		return physicianSpeciality;
	}
	
	public String getPhysicianAddress() {
		return physicianAddress;
	}
	
	public int getPrescription_type_id() {
		return prescription_type_id;
	}
	
	public String getPrescriptionDate() {
		return prescriptionDate;
	}
	
	public String getProfessional_physician_number_id() {
		return professional_physician_number_id;
	}
	
	public String getPatient_address() {
		return patient_address;
	}
	
	public String getTextContent() {
		return textContent;
	}
	
	public void setAccidentDate(String accidentDate) {
		this.accidentDate = accidentDate;
	}
	
	public void setAccidentNumber(String accidentNumber) {
		this.accidentNumber = accidentNumber;
	}
	
	public void setBilling_physician_number_id(String billing_physician_number_id) {
		this.billing_physician_number_id = billing_physician_number_id;
	}
	
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	public void setIncident_id(int incident_id) {
		this.incident_id = incident_id;
	}
	
	public void setSocial_id(String social_id) {
		this.social_id = social_id;
	}
	
	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}
	
	public void setPatientfirstName(String patientfirstName) {
		this.patientfirstName = patientfirstName;
	}
	
	public void setPatientMaidenName(String patientMaidenName) {
		this.patientMaidenName = patientMaidenName;
	}
	
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	
	public void setPhysician_id(long physician_id) {
		this.physician_id = physician_id;
	}
	
	public void setPhysicianFullName(String physicianFullName) {
		this.physicianFullName = physicianFullName;
	}
	
	public void setPhysicianSpeciality(String physicianSpeciality) {
		this.physicianSpeciality = physicianSpeciality;
	}
	
   public void setPatient_address(String patient_address) {
	 this.patient_address = patient_address;
   }
	
	public void setPrescription_id(long prescription_id) {
		this.prescription_id = prescription_id;
	}
	
	public void setPrescription_type_id(int prescription_type_id) {
		this.prescription_type_id = prescription_type_id;
	}
	
	public void setPrescriptionDate(String prescriptionDate) {
		this.prescriptionDate = prescriptionDate;
	}
	
	public void setProfessional_physician_number_id(String professional_physician_number_id) {
		this.professional_physician_number_id = professional_physician_number_id;
	}
	
	public void setPhysicianAddress(String physicianAddress) {
		this.physicianAddress = physicianAddress;
	}
	
	public void setTextContent(String textContent) {
		this.textContent = textContent;
	}
	
	public Physician getPhysician() {
		return physician;
	}
	
	public void setPhysician(Physician physician) {
		this.physician = physician;
	}
	
	@OneToOne(mappedBy="Incident")
	public Incident getIncident() {
		return incident;
	}
	
	public void setIncident(Incident incident) {
		this.incident = incident;
	}
	
	public Prescription_type getPrescription_type() {
		return prescription_type;
	}
	
	public void setPrescription_type(Prescription_type prescription_type) {
		this.prescription_type = prescription_type;
	}
}
