/**
 * 
 */
package eu.myhippocrates.drug.module;

/**
 * @author Gilles MARIA SUBE
 *
 */

import	java.io.Serializable;

import javax.persistence.CascadeType;
import	javax.persistence.Column;
import	javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import	javax.xml.bind.annotation.XmlAccessorType;
import	javax.xml.bind.annotation.XmlRootElement;
import	javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {""})
@XmlRootElement(name = "templatedrugs")
@Entity @Table(name = "templatedrugs")
@NamedQuery(name="TemplateDrugs.findAll", query="SELECT tmdr FROM TemplateDrugs tmdr")

public class TemplateDrugs implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="template_drugs_id", nullable=false)
	private	long	template_drugs_id;
	@Column(name="template_id", nullable=false)
	private	long	template_id;
	@Column(name="name", nullable=false)
	private	String	name;
	@Column(name="drug_id", nullable=false)
	private	int	drug_id;
	@Column(name="drug_list_id", nullable=false)
	private	int	drug_list_id;
	@Column(name="posology", nullable=false)
	private	String	posology;
	
	private	PrescriptionTemplate	prescriptionTemplate;
	
	public TemplateDrugs() {
		// TODO Auto-generated constructor stub
	}
	
	public int getDrug_id() {
		return drug_id;
	}
	
	public int getDrug_list_id() {
		return drug_list_id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getPosology() {
		return posology;
	}
	
	public long getTemplate_drugs_id() {
		return template_drugs_id;
	}
	
	public long getTemplate_id() {
		return template_id;
	}
	
	public void setDrug_id(int drug_id) {
		this.drug_id = drug_id;
	}
	
	public void setDrug_list_id(int drug_list_id) {
		this.drug_list_id = drug_list_id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setPosology(String posology) {
		this.posology = posology;
	}
	
	public void setTemplate_drugs_id(long template_drugs_id) {
		this.template_drugs_id = template_drugs_id;
	}
	
	public void setTemplate_id(long template_id) {
		this.template_id = template_id;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@OneToOne(mappedBy="PrescriptionTemplate")
	public PrescriptionTemplate getPrescriptionTemplate() {
		return prescriptionTemplate;
	}
	
	public void setPrescriptionTemplate(PrescriptionTemplate prescriptionTemplate) {
		this.prescriptionTemplate = prescriptionTemplate;
	}

}
