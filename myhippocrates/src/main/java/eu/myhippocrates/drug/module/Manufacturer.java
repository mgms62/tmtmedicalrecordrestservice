/**
 * 
 */
package eu.myhippocrates.drug.module;

/**
 * @author Gilles MARIA SUBE
 *
 */

import	java.io.Serializable;
import	javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import	javax.xml.bind.annotation.XmlAccessorType;
import	javax.xml.bind.annotation.XmlRootElement;
import	javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {})
@XmlRootElement(name = "manufacturer")
@Entity @Table(name = "manufacturer")
@NamedQuery(name="Manufacturer.findAll", query="SELECT m FROM Manufacturer m")

public class Manufacturer implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public Manufacturer() {
		// TODO Auto-generated constructor stub
	}
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="manufacturer_id")
	private	int		manufacturer_id;
	@Column(name="number")
	private String number;
	@Column(name="name")
	private	String	name;
	@Column(name="address")
	private	String	address;
	@Column(name="spec")
	private String spec;
	@Column (name="from")
	private String from;
	@Column(name="to")
	private String to;
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public int getManufacturer_id() {
		return manufacturer_id;
	}
	
	public String getNumber() {
		return number;
	}
	
	public String getName() {
		return name;
	}
	
	public String getAddress() {
		return address;
	}
	
	public String getSpec() {
		return spec;
	}
	
	public String getFrom() {
		return from;
	}
	
	public String getTo() {
		return to;
	}
	
	public void setManufacturer_id(int manufacturer_id) {
		this.manufacturer_id = manufacturer_id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}

	public void setSpec(String spec) {
		this.spec = spec;
	}
	
	public void setFrom(String from) {
		this.from = from;
	}
	
	public void setTo(String to) {
		this.to = to;
	}
}
