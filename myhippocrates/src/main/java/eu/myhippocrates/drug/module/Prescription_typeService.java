/**
 * 
 */
package eu.myhippocrates.drug.module;

/**
 * @author Gilles MARIA SUBE
 *
 */

import	java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Persistence;

import	javax.ws.rs.Consumes;
import	javax.ws.rs.DELETE;
import	javax.ws.rs.GET;
import	javax.ws.rs.POST;
import	javax.ws.rs.PUT;
import	javax.ws.rs.Path;
import	javax.ws.rs.PathParam;
import	javax.ws.rs.Produces;
import	javax.ws.rs.core.Response;



@Entity
@Table(name="prescription_type")
@NamedQueries({
	@NamedQuery(name="Prescription_type.findName", query="SELECT name from Prescription_type where prescription_type_id = :prescription_type_id")
})

public class Prescription_typeService {
		
		EntityManagerFactory	emf;
		EntityManager		em;
		EntityTransaction	et;
	
		public Prescription_typeService() {
		}

		@GET
		@Produces("application/json")
		public	Response getPrescription_type(@PathParam("prescription_type_id") int prescription_type_id) {
			emf = Persistence.createEntityManagerFactory("");
			em = emf.createEntityManager();
			Prescription_type	prescription_type = em.find(Prescription_type.class, prescription_type_id);
			em.createNamedQuery("Prescription_type.findName");
			et = em.getTransaction();
			et.begin();
			et.commit();

			return( Response.ok(prescription_type).build() );
		}


}
