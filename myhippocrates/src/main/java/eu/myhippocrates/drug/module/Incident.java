/**
 * 
 */
package eu.myhippocrates.drug.module;

/**
 * @author Gilles MARIA SUBE
 *
 */

import	java.io.Serializable;
import	javax.persistence.*;

import javax.xml.bind.annotation.XmlAccessType;
import	javax.xml.bind.annotation.XmlAccessorType;
import	javax.xml.bind.annotation.XmlRootElement;
import	javax.xml.bind.annotation.XmlType;

import eu.myhippocrates.database.Site;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {})
@XmlRootElement(name = "incident")
@Entity @Table(name = "incident")
@NamedQuery(name="Incident.findAll", query="SELECT i FROM Incident i")

public class Incident implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private	PatientData	patient;
	
	@ManyToOne
	@JoinColumn(name="incident_type_id", nullable=false, insertable=false, updatable=false)
	private Incident_type incident_type;
	
	@ManyToOne
	@JoinColumn(name="physician_id", nullable=false, insertable=false, updatable=false)
	private Physician	physician;
	
	@ManyToOne
	@JoinColumn(name="site_id", nullable=false, insertable=false, updatable=false)
	private	Site		site;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="incident_id", nullable=false)
	private	long	incident_id;
	@Column(name="patient_id", nullable=false)
	private	long	patient_id;
	@Column(name="physician_id")
	private	long	physician_id;
	@Column(name="incident_date", nullable=false)
	private	String	incident_date;
	@Column(name="created", nullable=false)
	private	String	created;
	@Column(name="created_by", nullable=false)
	private	long	created_by;
	@Column(name="modified")
	private	String	modified;
	@Column(name="modified_by")
	private	String	modified_by;
	@Column(name="is_visit", nullable=false)
	private	boolean	is_visit;
	@Column(name="is_accident")
	private	boolean	is_accident;
	@Column(name="accident_nr")
	private	String	accident_nr;
	@Column(name="accident_date")
	private	String	accident_date;
	@Column(name="site_id")
	private	long	site_id;
	@Column(name="incident_type_id")
	private	int		incident_type_id;
	@Column(name="entry_date")
	private String entry_date;
	@Column(name="text_content")
	private	String	text_content;
	@Column(name="filename")
	private String	filename;
	@Column(name="filesize")
	private long	filesize;
	@Column(name="sick_leave_start_date")
	private	String	sick_leave_start_date;
	@Column(name="sick_leave_end_date")
	private	String	sick_leave_end_date;
	
	public Incident() {
		// TODO Auto-generated constructor stub
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public long getIncident_id() {
		return incident_id;
	}
	
	public int getIncident_type_id() {
		return incident_type_id;
	}
	
	public long getPatient_id() {
		return patient_id;
	}
	
	public long getPhysician_id() {
		return physician_id;
	}
	
	public String getIncident_date() {
		return incident_date;
	}
	
	public String getCreated() {
		return created;
	}
	
	public long getCreated_by() {
		return created_by;
	}
	
	public String getModified() {
		return modified;
	}
	
	public String getModified_by() {
		return modified_by;
	}
	
	public boolean isIs_visit() {
		return is_visit;
	}
	
	public boolean isIs_accident() {
		return is_accident;
	}
	
	public String getAccident_nr() {
		return accident_nr;
	}
	
	public String getAccident_date() {
		return accident_date;
	}
	
	public long getSite_id() {
		return site_id;
	}
	
	public PatientData getPatient() {
		return patient;
	}
	
	public String getEntry_date() {
		return entry_date;
	}
	
	public String getFilename() {
		return filename;
	}
	
	public long getFilesize() {
		return filesize;
	}
	
	public String getText_content() {
		return text_content;
	}
	
	public String getSick_leave_start_date() {
		return sick_leave_start_date;
	}
	
	public String getSick_leave_end_date() {
		return sick_leave_end_date;
	}
	
	public void setIncident_id(long incident_id) {
		this.incident_id = incident_id;
	}
	
	public void setPatient_id(long patient_id) {
		this.patient_id = patient_id;
	}
	
	public void setPhysician_id(long physician_id) {
		this.physician_id = physician_id;
	}
	
	public void setIncident_date(String incident_date) {
		this.incident_date = incident_date;
	}
	
	public void setCreated(String created) {
		this.created = created;
	}
	
	public void setCreated_by(long created_by) {
		this.created_by = created_by;
	}
	
	public void setModified(String modified) {
		this.modified = modified;
	}
	
	public void setModified_by(String modified_by) {
		this.modified_by = modified_by;
	}
	
	public void setIs_visit(boolean is_visit) {
		this.is_visit = is_visit;
	}
	
	public void setIs_accident(boolean is_accident) {
		this.is_accident = is_accident;
	}
	
	public void setAccident_nr(String accident_nr) {
		this.accident_nr = accident_nr;
	}
	
	public void setAccident_date(String accident_date) {
		this.accident_date = accident_date;
	}
	
	public void setSite_id(long site_id) {
		this.site_id = site_id;
	}
	
	public void setIncident_type_id(int incident_type_id) {
		this.incident_type_id = incident_type_id;
	}
	
	//one to one relation (FK)
	@OneToOne(mappedBy="PatientData", cascade=CascadeType.ALL)
	public PatientData getPatientData() {
		return patient;
	}
	

	public Physician getPhysician() {
		return physician;
	}
	

	public Site getSite() {
		return site;
	}
	
	public void setPatientData(PatientData patient) {
		this.patient = patient;
	}
	
	public void setPhysician(Physician physician) {
		this.physician = physician;
	}
	
	public void setSite(Site site) {
		this.site = site;
	}
	
	public void setText_content(String text_content) {
		this.text_content = text_content;
	}
	
	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	public void setFilesize(long filesize) {
		this.filesize = filesize;
	}
	
	public void setEntry_date(String entry_date) {
		this.entry_date = entry_date;
	}
	
	public void setSick_leave_end_date(String sick_leave_end_date) {
		this.sick_leave_end_date = sick_leave_end_date;
	}
	
	public void setSick_leave_start_date(String sick_leave_start_date) {
		this.sick_leave_start_date = sick_leave_start_date;
	}
	
	public void setPatient(PatientData patient) {
		this.patient = patient;
	}
}
