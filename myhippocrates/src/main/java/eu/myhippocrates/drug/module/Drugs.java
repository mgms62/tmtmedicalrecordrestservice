/**
 * 
 */
package eu.myhippocrates.drug.module;

/**
 * @author Gilles MARIA SUBE
 *
 */

import	java.io.Serializable;

import javax.persistence.Column;
import	javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import	javax.persistence.Table;
import	javax.persistence.NamedQuery;

import	javax.xml.bind.annotation.XmlAccessType;
import	javax.xml.bind.annotation.XmlAccessorType;
import	javax.xml.bind.annotation.XmlAttribute;
import	javax.xml.bind.annotation.XmlElement;
import	javax.xml.bind.annotation.XmlRootElement;
import	javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = " ", propOrder = {})
@XmlRootElement(name = "drugs")
@Entity @Table(name = "drugs")
@NamedQuery(name="Drugs.findAll", query="SELECT d FROM Drugs d")

public class Drugs implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="drugs_id", nullable=false)
	private	long	drugs_id;
	@Column(name="drug_lists_id")
	private	int		drug_list_id;
	@Column(name="titular_number")
	private	int		titular_number;
	@Column(name="fabricant_number")
	private	int		fabricant_number;
	@Column(name="short_commercial_denom")
	private	String	short_commercial_denom;
	@Column(name="commercial_denom")
	private	String	commercial_denom;
	@Column(name="form")
	private	String	form;
	@Column(name="dosage_qty1")
	private	int		dosage_qty1;
	@Column(name="dosage_unit1")
	private	String	dosage_unit1;
	@Column(name="drug_qty2")
	private	int		dosage_qty2;
	@Column(name="dosage_unit2")
	private	String	dosage_unit2;
	@Column(name="content")
	private	String	content;
	@Column(name="price")
	private	String	price;
	@Column(name="currency")
	private String currency;
	@Column(name="substance1_denom")
	private	String	substance1_denom;
	@Column(name="code_atc1")
	private String code_atc1;
	@Column(name="substance1_qty1")
	private	String	substance1_qty1;
	@Column(name="substance1_unit1")
	private	String	substance1_unit1;
	@Column(name="substance1_qty2")
	private	String	substance1_qty2;
	@Column(name="substance1_unit2")
	private	String	substance1_unit2;
	@Column(name="substance2_denom")
	private	String	substance2_denom;
	@Column(name="code_atc2")
	private String code_atc2;
	@Column(name="substance2_qty1")
	private	String	substance2_qty1;
	@Column(name="substance2_unit1")
	private	String	substance2_unit1;
	@Column(name="substance2_qty2")
	private	String	substance2_qty2;
	@Column(name="substance2_unit2")
	private	String	substance2_unit2;
	@Column(name="substance3_denom")
	private	String	substance3_denom;
	@Column(name="code_atc3")
	private String code_atc3;
	@Column(name="substance3_qty1")
	private	String	substance3_qty1;
	@Column(name="substance3_unit1")
	private	String	substance3_unit1;
	@Column(name="substance3_qty2")
	private	String	substance3_qty2;
	@Column(name="substance3_unit2")
	private	String	substance3_unit2;
	@Column(name="substance4_denom")
	private	String	substance4_denom;
	@Column(name="code_atc4")
	private String code_atc4;
	@Column(name="substance4_qty1")
	private	String	substance4_qty1;
	@Column(name="substance4_unit1")
	private	String	substance4_unit1;
	@Column(name="substance4_qty2")
	private	String	substance4_qty2;
	@Column(name="substance4_unit2")
	private	String	substance4_unit2;
	@Column(name="substance5_denom")
	private	String	substance5_denom;
	@Column(name="code_atc5")
	private String code_atc5;
	@Column(name="substance5_qty1")
	private	String	substance5_qty1;
	@Column(name="substance5_unit1")
	private	String	substance5_unit1;
	@Column(name="substance5_qty2")
	private	String	substance5_qty2;
	@Column(name="substance5_unit2")
	private	String	substance5_unit2;
	@Column(name="substance6_denom")
	private	String	substance6_denom;
	@Column(name="code_atc6")
	private String code_atc6;
	@Column(name="substance6_qty1")
	private	String	substance6_qty1;
	@Column(name="substance6_unit1")
	private	String	substance6_unit1;
	@Column(name="substance6_qty2")
	private	String	substance6_qty2;
	@Column(name="substance6_unit2")
	private	String	substance6_unit2;
	@Column(name="substance7_denom")
	private	String	substance7_denom;
	@Column(name="code_atc7")
	private String code_atc7;
	@Column(name="substance7_qty1")
	private	String	substance7_qty1;
	@Column(name="substance7_unit1")
	private	String	substance7_unit1;
	@Column(name="substance7_qty2")
	private	String	substance7_qty2;
	@Column(name="substance7_unit2")
	private	String	substance7_unit2;
	@Column(name="substance8_denom")
	private	String	substance8_denom;
	@Column(name="code_atc8")
	private String code_atc8;
	@Column(name="substance8_qty1")
	private	String	substance8_qty1;
	@Column(name="substance8_unit1")
	private	String	substance8_unit1;
	@Column(name="substance8_qty2")
	private	String	substance8_qty2;
	@Column(name="substance8_unit2")
	private	String	substance8_unit2;
	@Column(name="substance9_denom")
	private	String	substance9_denom;
	@Column(name="code_atc9")
	private String code_atc9;
	@Column(name="substance9_qty1")
	private	String	substance9_qty1;
	@Column(name="substance9_unit1")
	private	String	substance9_unit1;
	@Column(name="substance9_qty2")
	private	String	substance9_qty2;
	@Column(name="substance9_unit2")
	private	String	substance9_unit2;
	@Column(name="substance10_denom")
	private	String	substance10_denom;
	@Column(name="code_atc10")
	private String code_atc10;
	@Column(name="substance10_qty1")
	private	String	substance10_qty1;
	@Column(name="substance10_unit1")
	private	String	substance10_unit1;
	@Column(name="substance10_qty2")
	private	String	substance10_qty2;
	@Column(name="substance10_unit2")
	private	String	substance10_unit2;
	
	/*@ManyToOne
	@JoinColumn(name="drug_list_id", nullable=false, insertable=false, updatable=false)
	private	Drug_lists	drug_lists;*/
	
	public Drugs() {
		// TODO Auto-generated constructor stub
	}

	public long getDrugs_id() {
		return drugs_id;
	}
	
	public int getDrug_list_id() {
		return drug_list_id;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public String getShort_commercial_denom() {
		return short_commercial_denom;
	}
	
	public String getCommercial_denom() {
		return commercial_denom;
	}
	
public int getTitular_number() {
	return titular_number;
}

public int getFabricant_number() {
	return fabricant_number;
}
	
	public String getContent() {
		return content;
	}
	
	public String getForm() {
		return form;
	}
	
	public int getDosage_qty1() {
		return dosage_qty1;
	}
	
	public String getDosage_unit1() {
		return dosage_unit1;
	}
	
	public int getDosage_qty2() {
		return dosage_qty2;
	}
	
	public String getDosage_unit2() {
		return dosage_unit2;
	}
	
	public String getPrice() {
		return price;
	}
	
	public String getCurrency() {
		return currency;
	}
	
	public String getSubstance1_denom() {
		return substance1_denom;
	}
	
	public String getCode_atc1() {
		return code_atc1;
	}
	
	public String getSubstance1_qty1() {
		return substance1_qty1;
	}
	
	public String getSubstance1_unit1() {
		return substance1_unit1;
	}
	
	public String getSubstance1_qty2() {
		return substance1_qty2;
	}
	
	public String getSubstance1_unit2() {
		return substance1_unit2;
	}
	
	public String getSubstance2_denom() {
		return substance2_denom;
	}
	
	public String getCode_atc2() {
		return code_atc2;
	}
	
	public String getSubstance2_qty1() {
		return substance2_qty1;
	}
	
	public String getSubstance2_unit1() {
		return substance2_unit1;
	}
	
	public String getSubstance2_qty2() {
		return substance2_qty2;
	}
	
	public String getSubstance2_unit2() {
		return substance2_unit2;
	}
	
	public String getSubstance3_denom() {
		return substance3_denom;
	}
	
public String getCode_atc3() {
	return code_atc3;
}
	
	public String getSubstance3_qty1() {
		return substance3_qty1;
	}
	
	public String getSubstance3_unit1() {
		return substance3_unit1;
	}
	
	public String getSubstance3_qty2() {
		return substance3_qty2;
	}
	
	public String getSubstance3_unit2() {
		return substance3_unit2;
	}
	
	public String getSubstance4_denom() {
		return substance4_denom;
	}
	
	public String getCode_atc4() {
		return code_atc4;
	}
	
	public String getSubstance4_qty1() {
		return substance4_qty1;
	}
	
	public String getSubstance4_unit1() {
		return substance4_unit1;
	}
	
	public String getSubstance4_qty2() {
		return substance4_qty2;
	}
	
	public String getSubstance4_unit2() {
		return substance4_unit2;
	}
	
	public String getSubstance5_denom() {
		return substance5_denom;
	}
	
public String getCode_atc5() {
	return code_atc5;
}
	
	public String getSubstance5_qty1() {
		return substance5_qty1;
	}
	
	public String getSubstance5_unit1() {
		return substance5_unit1;
	}
	
	public String getSubstance5_qty2() {
		return substance5_qty2;
	}
	
	public String getSubstance5_unit2() {
		return substance5_unit2;
	}
	
	public String getSubstance6_denom() {
		return substance6_denom;
	}
	
	public String getCode_atc6() {
		return code_atc6;
	}
	
	public String getSubstance6_qty1() {
		return substance6_qty1;
	}
	
	public String getSubstance6_unit1() {
		return substance6_unit1;
	}
	
	public String getSubstance6_qty2() {
		return substance6_qty2;
	}
	
	public String getSubstance6_unit2() {
		return substance6_unit2;
	}
	
	public String getSubstance7_denom() {
		return substance7_denom;
	}
	
	public String getCode_atc7() {
		return code_atc7;
	}
	
	public String getSubstance7_qty1() {
		return substance7_qty1;
	}
	
	public String getSubstance7_unit1() {
		return substance7_unit1;
	}
	
	public String getSubstance7_qty2() {
		return substance7_qty2;
	}
	
	public String getSubstance7_unit2() {
		return substance7_unit2;
	}
	
	public String getSubstance8_denom() {
		return substance8_denom;
	}
	
	public String getSubstance8_qty1() {
		return substance8_qty1;
	}
	
	public String getSubstance8_unit1() {
		return substance8_unit1;
	}
	
	public String getCode_atc8() {
		return code_atc8;
	}
	
	public String getSubstance8_qty2() {
		return substance8_qty2;
	}
	
	public String getSubstance8_unit2() {
		return substance8_unit2;
	}
	
	public String getSubstance9_denom() {
		return substance9_denom;
	}
	
	public String getCode_atc9() {
		return code_atc9;
	}
	
	public String getSubstance9_qty1() {
		return substance9_qty1;
	}
	
	public String getSubstance9_unit1() {
		return substance9_unit1;
	}
	
	public String getSubstance9_qty2() {
		return substance9_qty2;
	}
	
	public String getSubstance9_unit2() {
		return substance9_unit2;
	}
	
	public String getSubstance10_denom() {
		return substance10_denom;
	}
	
	public String getCode_atc10() {
		return code_atc10;
	}
	
	public String getSubstance10_qty1() {
		return substance10_qty1;
	}
	
	public String getSubstance10_unit1() {
		return substance10_unit1;
	}
	
	public String getSubstance10_qty2() {
		return substance10_qty2;
	}
	
	public String getSubstance10_unit2() {
		return substance10_unit2;
	}
	
	public void setDrugs_id(long drugs_id) {
		this.drugs_id = drugs_id;
	}
	
	public void setDrug_list_id(int drug_list_id) {
		this.drug_list_id = drug_list_id;
	}
	
	public void setTitular_number(int titular_number) {
		this.titular_number = titular_number;
	}
	
	public void setFabricant_number(int fabricant_number) {
		this.fabricant_number = fabricant_number;
	}
	
	public void setShort_commercial_denom(String short_commercial_denom) {
		this.short_commercial_denom = short_commercial_denom;
	}
	
	public void setCommercial_denom(String commercial_denom) {
		this.commercial_denom = commercial_denom;
	}
	
	public void setForm(String form) {
		this.form = form;
	}
	
	public void setDosage_qty1(int dosage_qty1) {
		this.dosage_qty1 = dosage_qty1;
	}
	
	public void setDosage_unit1(String dosage_unit1) {
		this.dosage_unit1 = dosage_unit1;
	}
	
	public void setDosage_qty2(int dosage_qty2) {
		this.dosage_qty2 = dosage_qty2;
	}
	
	public void setDosage_unit2(String dosage_unit2) {
		this.dosage_unit2 = dosage_unit2;
	}
	
	public void setPrice(String price) {
		this.price = price;
	}
	
	 public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public void setSubstance1_denom(String substance1_denom) {
		this.substance1_denom = substance1_denom;
	}
	
	public void setCode_atc1(String code_atc1) {
		this.code_atc1 = code_atc1;
	}
	
	public void setSubstance1_qty1(String substance1_qty1) {
		this.substance1_qty1 = substance1_qty1;
	}
	
	public void setSubstance1_unit1(String substance1_unit1) {
		this.substance1_unit1 = substance1_unit1;
	}
	
	public void setSubstance1_qty2(String substance1_qty2) {
		this.substance1_qty2 = substance1_qty2;
	}
	
	public void setSubstance1_unit2(String substance1_unit2) {
		this.substance1_unit2 = substance1_unit2;
	}
	
	public void setSubstance2_denom(String substance2_denom) {
		this.substance2_denom = substance2_denom;
	}
	
	public void setCode_atc2(String code_atc2) {
		this.code_atc2 = code_atc2;
	}
	
	public void setSubstance2_qty1(String substance2_qty1) {
		this.substance2_qty1 = substance2_qty1;
	}
	
	public void setSubstance2_unit1(String substance2_unit1) {
		this.substance2_unit1 = substance2_unit1;
	}
	
	public void setSubstance2_qty2(String substance2_qty2) {
		this.substance2_qty2 = substance2_qty2;
	}
	
	public void setSubstance2_unit2(String substance2_unit2) {
		this.substance2_unit2 = substance2_unit2;
	}
	
	public void setSubstance3_denom(String substance3_denom) {
		this.substance3_denom = substance3_denom;
	}
	
	public void setSubstance3_qty1(String substance3_qty1) {
		this.substance3_qty1 = substance3_qty1;
	}
	
	public void setCode_atc3(String code_atc3) {
		this.code_atc3 = code_atc3;
	}
	
	public void setSubstance3_unit1(String substance3_unit1) {
		this.substance3_unit1 = substance3_unit1;
	}
	
	public void setSubstance3_qty2(String substance3_qty2) {
		this.substance3_qty2 = substance3_qty2;
	}
	
	public void setSubstance3_unit2(String substance3_unit2) {
		this.substance3_unit2 = substance3_unit2;
	}
	
	public void setSubstance4_denom(String substance4_denom) {
		this.substance4_denom = substance4_denom;
	}
	
	public void setCode_atc4(String code_atc4) {
		this.code_atc4 = code_atc4;
	}
	
	public void setSubstance4_qty1(String substance4_qty1) {
		this.substance4_qty1 = substance4_qty1;
	}
	
	public void setSubstance4_unit1(String substance4_unit1) {
		this.substance4_unit1 = substance4_unit1;
	}
	
	public void setSubstance4_qty2(String substance4_qty2) {
		this.substance4_qty2 = substance4_qty2;
	}
	
	public void setSubstance4_unit2(String substance4_unit2) {
		this.substance4_unit2 = substance4_unit2;
	}
	
	public void setSubstance5_denom(String substance5_denom) {
		this.substance5_denom = substance5_denom;
	}
	
	public void setCode_atc5(String code_atc5) {
		this.code_atc5 = code_atc5;
	}
	
	public void setSubstance5_qty1(String substance5_qty1) {
		this.substance5_qty1 = substance5_qty1;
	}
	
	public void setSubstance5_unit1(String substance5_unit1) {
		this.substance5_unit1 = substance5_unit1;
	}
	
	public void setSubstance5_qty2(String substance5_qty2) {
		this.substance5_qty2 = substance5_qty2;
	}
	
	public void setSubstance5_unit2(String substance5_unit2) {
		this.substance5_unit2 = substance5_unit2;
	}
	
	public void setSubstance6_denom(String substance6_denom) {
		this.substance6_denom = substance6_denom;
	}
	
	public void setCode_atc6(String code_atc6) {
		this.code_atc6 = code_atc6;
	}
	
	public void setSubstance6_qty1(String substance6_qty1) {
		this.substance6_qty1 = substance6_qty1;
	}
	
	public void setSubstance6_unit1(String substance6_unit1) {
		this.substance6_unit1 = substance6_unit1;
	}
	
	public void setSubstance6_qty2(String substance6_qty2) {
		this.substance6_qty2 = substance6_qty2;
	}
	
	public void setSubstance6_unit2(String substance6_unit2) {
		this.substance6_unit2 = substance6_unit2;
	}
	
	public void setSubstance7_denom(String substance7_denom) {
		this.substance7_denom = substance7_denom;
	}
	
	public void setCode_atc7(String code_atc7) {
		this.code_atc7 = code_atc7;
	}
	
	public void setSubstance7_qty1(String substance7_qty1) {
		this.substance7_qty1 = substance7_qty1;
	}
	
	public void setSubstance7_unit1(String substance7_unit1) {
		this.substance7_unit1 = substance7_unit1;
	}
	
	public void setSubstance7_qty2(String substance7_qty2) {
		this.substance7_qty2 = substance7_qty2;
	}
	
	public void setSubstance7_unit2(String substance7_unit2) {
		this.substance7_unit2 = substance7_unit2;
	}
	
	public void setSubstance8_denom(String substance8_denom) {
		this.substance8_denom = substance8_denom;
	}
	
	public void setCode_atc8(String code_atc8) {
		this.code_atc8 = code_atc8;
	}
	
	public void setSubstance8_qty1(String substance8_qty1) {
		this.substance8_qty1 = substance8_qty1;
	}
	
	public void setSubstance8_unit1(String substance8_unit1) {
		this.substance8_unit1 = substance8_unit1;
	}
	
	public void setSubstance8_qty2(String substance8_qty2) {
		this.substance8_qty2 = substance8_qty2;
	}
	
	public void setSubstance8_unit2(String substance8_unit2) {
		this.substance8_unit2 = substance8_unit2;
	}
	
	public void setSubstance9_denom(String substance9_denom) {
		this.substance9_denom = substance9_denom;
	}
	
	public void setCode_atc9(String code_atc9) {
		this.code_atc9 = code_atc9;
	}
	
	public void setSubstance9_qty1(String substance9_qty1) {
		this.substance9_qty1 = substance9_qty1;
	}
	
	public void setSubstance9_unit1(String substance9_unit1) {
		this.substance9_unit1 = substance9_unit1;
	}
	
	public void setSubstance9_qty2(String substance9_qty2) {
		this.substance9_qty2 = substance9_qty2;
	}
	
	public void setSubstance9_unit2(String substance9_unit2) {
		this.substance9_unit2 = substance9_unit2;
	}
	
	public void setSubstance10_denom(String substance10_denom) {
		this.substance10_denom = substance10_denom;
	}
	
	public void setCode_atc10(String code_atc10) {
		this.code_atc10 = code_atc10;
	}
	
	public void setSubstance10_qty1(String substance10_qty1) {
		this.substance10_qty1 = substance10_qty1;
	}
	
	public void setSubstance10_unit1(String substance10_unit1) {
		this.substance10_unit1 = substance10_unit1;
	}
	
	public void setSubstance10_qty2(String substance10_qty2) {
		this.substance10_qty2 = substance10_qty2;
	}
	
	public void setSubstance10_unit2(String substance10_unit2) {
		this.substance10_unit2 = substance10_unit2;
	}
	
}
