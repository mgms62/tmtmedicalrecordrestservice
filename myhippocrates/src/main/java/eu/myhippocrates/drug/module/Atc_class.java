/**
 * 
 */
package eu.myhippocrates.drug.module;

/**
 * @author Gilles MARIA SUBE
 *
 */

import	java.io.Serializable;

import	javax.persistence.Column;
import	javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import	javax.xml.bind.annotation.XmlAccessorType;
import	javax.xml.bind.annotation.XmlRootElement;
import	javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {})
@XmlRootElement(name = "atc_class")
@Entity @Table(name = "atc_class")
@NamedQuery(name="Atc_class.findAll", query="SELECT atc FROM Atc_class atc")

public class Atc_class implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="atc_class_id", nullable=false)
	private	int		atc_class_id;
	@Column(name="atc_code")
	private	String	atc_code;
	@Column(name="name")
	private	String	name;
	@Column(name="parent")
	private	int		parent;
	
	public Atc_class() {
	}
	
	public int getAtc_class_id() {
		return atc_class_id;
	}
	
	public String getAtc_code() {
		return atc_code;
	}
	
	public String getName() {
		return name;
	}
	
	public int getParent() {
		return parent;
	}
	
	public void setAtc_class_id(int atc_class_id) {
		this.atc_class_id = atc_class_id;
	}
	
	public void setAtc_code(String atc_code) {
		this.atc_code = atc_code;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setParent(int parent) {
		this.parent = parent;
	}
	
}
