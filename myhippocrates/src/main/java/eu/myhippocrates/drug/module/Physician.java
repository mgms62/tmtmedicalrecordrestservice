/**
 * 
 */
package eu.myhippocrates.drug.module;

/**
 * @author Gilles MARIA SUBE
 *
 */

import	java.io.Serializable;

import	javax.persistence.Column;
import	javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import	javax.xml.bind.annotation.XmlAccessorType;
import	javax.xml.bind.annotation.XmlRootElement;
import	javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {})
@XmlRootElement(name = "physician")
@Entity @Table(name = "physician")
@NamedQuery(name="Physician.findByName", query="SELECT p FROM Physician p")

public class Physician implements Serializable {
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="physician_id", nullable=false)
	private	long	physician_id;
	@Column(name="ident_id")
	private	long	ident_id;
	@Column(name="name")
	private	String	name;
	@Column(name="first_name")
	private	String	first_name;
	@Column(name="professional_physician_number_id")
	private	String	professional_physician_number_id;
	@Column(name="billing_physician_number_id")
	private	String	billing_physician_number_id;
	@Column(name="speciality")
	private	String	speciality;
	@Column(name="street")
	private	String	street;
	@Column(name="street_number")
	private	String	street_number;
	@Column(name="locality")
	private	String	locality;
	@Column(name="adm_unit_id")
	private	String	adm_unit_id;
	@Column(name="country")
	private	String	country;
	@Column(name="post_code")
	private	String	post_code;
	@Column(name="email")
	private	String	email;
	@Column(name="title")
	private	String	title;
	@Column(name="professional_phone_number")
	private	String	professional_phone_number;
	@Column(name="private_phone_number")
	private	String	private_phone_number;
	@Column(name="fax_number")
	private	String	fax_number;
	@Column(name="mobile_number")
	private	String	mobile_number;
	
	private static final long serialVersionUID = 1L;
	
	public Physician() {
		// TODO Auto-generated constructor stub
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public long getPhysician_id() {
		return physician_id;
	}
	
	public long getIdent_id() {
		return ident_id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getFirst_name() {
		return first_name;
	}
	
	public String getProfessional_physician_number_id() {
		return professional_physician_number_id;
	}
	
	public String getBilling_physician_number_id() {
		return billing_physician_number_id;
	}
	
	public String getSpeciality() {
		return speciality;
	}
	
	public String getStreet() {
		return street;
	}
	
	public String getStreet_number() {
		return street_number;
	}
	
	public String getLocality() {
		return locality;
	}
	
	public String getAdm_unit_id() {
		return adm_unit_id;
	}
	
	public String getCountry() {
		return country;
	}
	
	public String getPost_code() {
		return post_code;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getPrivate_phone_number() {
		return private_phone_number;
	}
	
	public String getProfessional_phone_number() {
		return professional_phone_number;
	}
	
	public String getFax_number() {
		return fax_number;
	}
	
	public String getMobile_number() {
		return mobile_number;
	}
	
	public void setPhysician_id(long physician_id) {
		this.physician_id = physician_id;
	}
	
	public void setIdent_id(long ident_id) {
		this.ident_id = ident_id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	
	public void setProfessional_physician_number_id(String professional_physician_number_id) {
		this.professional_physician_number_id = professional_physician_number_id;
	}
	
	public void setBilling_physician_number_id(String billing_physician_number_id) {
		this.billing_physician_number_id = billing_physician_number_id;
	}
	
	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	public void setStreet_number(String street_number) {
		this.street_number = street_number;
	}
	
	public void setLocality(String locality) {
		this.locality = locality;
	}
	
	public void setAdm_unit_id(String adm_unit_id) {
		this.adm_unit_id = adm_unit_id;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public void setPost_code(String post_code) {
		this.post_code = post_code;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setPrivate_phone_number(String private_phone_number) {
		this.private_phone_number = private_phone_number;
	}
	
	public void setProfessional_phone_number(String professional_phone_number) {
		this.professional_phone_number = professional_phone_number;
	}
	
	public void setFax_number(String fax_number) {
		this.fax_number = fax_number;
	}
	
	public void setMobile_number(String mobile_number) {
		this.mobile_number = mobile_number;
	}
	
}
