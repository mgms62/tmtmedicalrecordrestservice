/**
 * 
 */
package eu.myhippocrates.drug.module;

/**
 * @author Gilles MARIA SUBE
 *
 */

import	java.io.Serializable;

import	javax.persistence.Column;
import	javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import	javax.xml.bind.annotation.XmlAccessorType;
import	javax.xml.bind.annotation.XmlRootElement;
import	javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {})
@XmlRootElement(name = "drug_lists")
@Entity @Table(name = "drug_lists")
@NamedQuery(name="Drug_lists.findAll", query="SELECT dl FROM Drug_lists dl")

public class Drug_lists implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="drug_lists_id", nullable=false)
	private	int		drug_lists_id;
	@Column(name="name")
	private	String	name;
	@Column(name="countryOrigin")
	private	String	countryOrigin;
	@Column(name="listDate")
	private	String	listDate;


public Drug_lists() {
	
	
	
	
}


public String getCountryOrigin() {
	return countryOrigin;
}

public String getListDate() {
	return listDate;
}

public String getName() {
	return name;
}

public int getDrug_lists_id() {
	return drug_lists_id;
}

public void setCountryOrigin(String countryOrigin) {
	this.countryOrigin = countryOrigin;
}

public void setDrug_lists_id(int drug_lists_id) {
	this.drug_lists_id = drug_lists_id;
}

public void setListDate(String listDate) {
	this.listDate = listDate;
}

public void setName(String name) {
	this.name = name;
}

}