/**
 * 
 */
package eu.myhippocrates.drug.module;

/**
 * @author Gilles MARIA SUBE
 *
 */

import	java.io.Serializable;

import javax.persistence.CascadeType;
import	javax.persistence.Column;
import	javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import	javax.xml.bind.annotation.XmlAccessorType;
import	javax.xml.bind.annotation.XmlRootElement;
import	javax.xml.bind.annotation.XmlType;

import eu.myhippocrates.database.User;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {""})
@XmlRootElement(name = "printprescription")
@Entity @Table(name = "printprescription")
@NamedQuery(name="PrintPrescription.findAll", query="SELECT pp FROM PrintPrescription pp")

public class PrintPrescription {

	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="print_prescription_id", nullable=false)
	private		long	print_prescription_id;
	@Column(name="prescription_id", nullable=false)
	private		long	prescription_id;
	@Column(name="printed_by_id", nullable=false)
	private		int		printed_by_id;
	@Column(name="print_date", nullable=false)
	private		String	print_date;
	@Column(name="is_copy")
	private		boolean	is_copy;
	
	@ManyToOne
	@JoinColumn(name="user_id", nullable=false, insertable=false, updatable=false)
	private	User			user;
	
	@ManyToOne
	@JoinColumn(name="prescription_id", nullable=false, insertable=false, updatable=false)
	private	Prescriptions	prescription;
	
	public PrintPrescription() {
		// TODO Auto-generated constructor stub
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public long getPrint_prescription_id() {
		return print_prescription_id;
	}
	
	public long getPrescription_id() {
		return prescription_id;
	}
	
	public int getPrinted_by_id() {
		return printed_by_id;
	}
	
	public String getPrint_date() {
		return print_date;
	}

	public boolean isIs_copy() {
		return is_copy;
	}
	
	public void setPrint_prescription_id(long print_prescription_id) {
		this.print_prescription_id = print_prescription_id;
	}
	
	public void setPrescription_id(long prescription_id) {
		this.prescription_id = prescription_id;
	}
	
	public void setPrint_date(String print_date) {
		this.print_date = print_date;
	}
	
	public void setPrinted_by_id(int printed_by_id) {
		this.printed_by_id = printed_by_id;
	}
	
	public void setIs_copy(boolean is_copy) {
		this.is_copy = is_copy;
	}
	
	public User getUser() {
		return this.user;
	}
	
	public Prescriptions getPrescription() {
		return this.prescription;
	}
	
	
	public void setUser(User user) {
		this.user = user;
	}
	
	
	public void setPrescription(Prescriptions prescription) {
		this.prescription = prescription;
	}
}
