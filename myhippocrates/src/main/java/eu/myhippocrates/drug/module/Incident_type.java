/**
 * 
 */
package eu.myhippocrates.drug.module;

/**
 * @author Gilles MARIA SUBE
 *
 */

import	java.io.Serializable;
import	javax.persistence.*;

import javax.xml.bind.annotation.XmlAccessType;
import	javax.xml.bind.annotation.XmlAccessorType;
import	javax.xml.bind.annotation.XmlRootElement;
import	javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {})
@XmlRootElement(name = "incident_type")
@Entity @Table(name = "incident_type")
@NamedQuery(name="Incident_type.findAll", query="SELECT it FROM Incident_type it")

public class Incident_type implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue
	@Column(name="incident_type_id", nullable=false)
	private	int		incident_type_id;
	@Column(name="name")
	private	String	name;
	@Column(name="mime_type")
	private	String	mime_type;
	
	public Incident_type() {
		// TODO Auto-generated constructor stub
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public int getIncident_type_id() {
		return incident_type_id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getMime_type() {
		return mime_type;
	}
	
	public void setIncident_type_id(int incident_type_id) {
		this.incident_type_id = incident_type_id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setMime_type(String mime_type) {
		this.mime_type = mime_type;
	}

}
