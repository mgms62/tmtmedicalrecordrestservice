/**
 * 
 */
package eu.myhippocrates.utils;

/**
 * @author Gilles MARIA SUBE
 *
 */


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class IdGenerator {
	private static class Generator{
		private final Long head;
		private volatile Short tail;
		public Generator(final String url, final String user, final String password) throws SQLException{
			Connection connection=DriverManager.getConnection(url, user, password);
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT NEXT VALUE FOR test_seq");
			
			resultSet.next();
			head=resultSet.getLong(1)<<16;
			tail=0;
			
			resultSet.close();
			statement.close();
			connection.close();
		}
		
		public Long generateId(){
			//Long start=(new GregorianCalendar(2015, 9, 12)).getTimeInMillis();  // 12 oct 2015 month starting at 0
			//Long id = (System.currentTimeMillis()-(start ^ (1L<<40))) << 23; // being Long.MIN_VALUE in 2015 overflow in +/- 2084
			Long result=head | (tail & 0xFFFF);
			tail++;
			return result;
		}
	}
			
	private static final BlockingQueue<Generator> pool = new LinkedBlockingQueue<>(); 
	private static final Logger logger = LogManager.getLogger(IdGenerator.class);
	private static volatile String url;
	private static volatile String user;
	private static volatile String password;
	//private static Long timeout;
	
	public static void idGenerator(Properties properties) throws SQLException, InterruptedException{
		url = properties.getProperty("db.url");
		user = properties.getProperty("db.user");
		password = properties.getProperty("db.password");
		//timeout = Long.valueOf(properties.getProperty("idGenerator.timeout","2000"));
		pool.put(new Generator(url, user, password));
	}
	
	public static Long getId() throws SQLException{
		Long result = null;
		try {
			Generator generator = pool.take();
			/*pool.poll(timeout, TimeUnit.MILLISECONDS);
			if (generator == null){
				throw new TimeoutException("");
			}*/
			result = generator.generateId();

			//as long as tail is not last number put generator back
			//Otherwise we need to create a new one.
			if((result & 0xFFFF) != 0xFFFF){  
				pool.put(generator);
			}else{
				pool.put(new Generator(url, user, password));
			}
		} catch (InterruptedException e) {
			logger.fatal("Interrupted during getID()");
			//should never happen unless we start to interrupt threads ourselves
		}
		return result;
	}
	
}
