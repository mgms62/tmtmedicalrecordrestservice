/**
 * 
 */
package eu.myhippocrates.weblistener;

/**
 * @author Gilles MARIA SUBE
 *
 */

import	javax.persistence.EntityManager;
import	javax.persistence.EntityManagerFactory;
import	javax.persistence.EntityTransaction;
import	javax.persistence.Persistence;
import	java.util.Properties;
import	java.io.InputStream;
import	java.io.IOException;
import	java.sql.SQLException;

import	javax.servlet.ServletContextEvent;
import	javax.servlet.ServletContextListener;
import	javax.servlet.annotation.WebListener;

import	org.apache.logging.log4j.LogManager;
import	org.apache.logging.log4j.Logger;

import eu.myhippocrates.utils.IdGenerator;

/*import	com.mongodb.MongoClient;
import	com.mongodb.MongoClientURI;*/

@WebListener
public	class PrescriptionContextListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		Logger	logger = LogManager.getLogger( this.getClass() );
		EntityManagerFactory emf = null;
		try {
			emf = Persistence.createEntityManagerFactory( "epr" );
		} catch( final Throwable t ) {
			Throwable tmp=t;
			while(tmp!=null){
				logger.info(tmp.toString());
				tmp=tmp.getCause();
			}
			throw t;
		}
		sce.getServletContext().setAttribute("EntityManagerFactory", emf);

		String idGeneratorPropertiesPath = sce.getServletContext().getInitParameter("IdGenerator_properties").toString();

		Properties	idGeneratorProperties = new Properties();
		try {
			final InputStream	idGeneratorPropertiesStream = sce.getServletContext().getResourceAsStream(idGeneratorPropertiesPath);
			idGeneratorProperties.load(idGeneratorPropertiesStream);
		} catch( final IOException ioe ) {
			logger.info("error loading idGenerator.properties: "+ioe.toString());
			throw new IllegalArgumentException("idGenerator properties could not be loaded", ioe);
		}
		try {
			IdGenerator idGenerator = new IdGenerator(idGeneratorProperties);
			sce.getServletContext().setAttribute("IdGenerator.Prescription", idGenerator);
		} catch( SQLException sqle ) {
			logger.info("error loading idGenerator.properties: " + sqle.toString());
			throw new IllegalArgumentException("idGenerator could not be initialized", sqle);
		} catch( InterruptedException ie ) {
			logger.info("error loading idGenerator.properties: " + ie.toString());
			Thread.currentThread().interrupt();
		}
	}

	@Override
	public	void contextDestroyed( ServletContextEvent sce ) {
		Logger	logger = LogManager.getLogger( this.getClass() );
		EntityManagerFactory emf = (EntityManagerFactory)sce.getServletContext().getAttribute("EntityManagerFactory");
		if (emf.isOpen()) emf.close();
		logger.info("Prescription Context: emf closed");
	}
}
